--- Entry point.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local lp = require("plugin.luaproc")

lp:get_alert_dispatcher():addEventListener("luaproc_alert", function(event)
	print("!!!!", event.message, event.payload)
end)
lp:get_alert_dispatcher():addEventListener("crash_report", function(event)
	print("OH NO, A CRASH!", event.payload)
end)
--lp.alert("FORKS", true)
lp.setnumworkers(2)
---[[
lp.newproc(function()
	require("system")

	print("Z", system.pathForFile("main.lua"))

--	luaproc.alert("SOCKS", false)
---[=[
	print(luaproc.is_waiting(), luaproc.is_main_state())
--]=]

--[=[
	local c = luaproc.wants_to_close
	print("BEGIN")
	for i = 1, 150 do
		print("!!!!!",i)
		for j = 1, 1e4 do
			if c() then
				return
			end
			for k = 1, 1e3 do
			end
		end
	end
--]=]
	print("DONE")
end)

lp.newproc(function()
		LUAPROC_ERROR = { report_method = "alert", cleanup = function(aa)
				return {} end
		}
--[=[
	print("!!!", luaproc.is_main_state())
	for i = 1, 1e15 do
		if luaproc.is_waiting() then
			print("YEAH!", i)
			break
		end
	end
	print("THEN")
--]=]
---[=[
	print("DUCK")
	luaproc.sleep(3000)
	print("QUACKS")
	luaproc.sleep(2000)
	print("A LOT")
--]=]
	for _, mod in ipairs{
		"easing", 
		"json", "dkjson",
		"lfs", "lpeg", "ltn12", "mime", "re", "sqlite3",
		"socket",
		"socket.core", "mime.core",
		"socket.url", "socket.mbox", "socket.smtp", "socket.http", "socket.headers", "socket.ftp", "socket.tp"
	} do
		print("MODULE: " .. mod)
		print("")
		local ok, m = pcall(require, mod)

		if ok then
			for k, v in pairs(m) do
				print(k, v)
			end
		end
		print("")
	end

--[=[
	local c = luaproc.wants_to_close
	print("eee")
	for i = 1, 50 do
		print("____",i)
		for j = 1, 1e4 do
			if c() then
				return
			end
			for k = 1, 1e5 do
			end
		end
	end
	print("fff")
--]=]
	a = b + c
end)

---[=[
print("?", lp.is_main_state())
timer.performWithDelay(500, function()
	lp.wait()
	print("FINALLY")
end)
--]=]

--]]
--[==[
Runtime:addEventListener("x64bridge_data", function(event)
	print("HI HO", event.what)
end)

require("plugin.x64bridge"):MakeMessageWindow()

local ex = require("x64bridge_ex")

-- create an additional worker
lp.setnumworkers( 2 )

local path = --"C:/Users/XS/Documents/Visual Studio 2013/Projects/SimpleProject/x64/Debug/SimpleProject.exe"

"C:/Users/XS/Desktop/DOD/third_party/lua/bin/wlua.exe"

ex.NewLuaProc(path, [[
	local s, err

	repeat
		s, err = bridge:Read()
	until err or #s > 0
	print("!",s)
]], { cmdline = ("%q"):format("lua -l dumb") })


if false then
-- create a new lua process
lp.newproc( [[
	package.cpath = ]] .. ("%q"):format(package.cpath) .. [[

	local x64bridge = require("plugin.x64bridge")

	-- create a communication channel
	luaproc.newchannel( "achannel" ) 

	-- create a receiver lua process
	luaproc.newproc( [=[
		-- receive and print a message
		for i = 1, 2 do
			print(i, luaproc.receive( "achannel" ))
		end
	]=] )

	local bridge = x64bridge.LaunchProcess("C:/Users/XS/Documents/Visual Studio 2013/Projects/SimpleProject/x64/Debug/SimpleProject.exe")
	if bridge then
		local s, err

		repeat
			s, err = bridge:Read()
		until err or #s > 0

		if s then
			luaproc.send("achannel", s)
		end
	end
 
	-- create a sender lua process
	luaproc.newproc( [=[
		-- send a message
		luaproc.send( "achannel", "hello world from luaproc" )
	]=] )
]] )
end
--]==]