--- Extensions for X64 bridge.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Standard library imports --
local open = io.open
local type = type

-- Modules --
local crypto = require("crypto")
local lp_ok, luaproc = pcall(require, "plugin.luaproc")

-- Corona globals --
local system = system

-- Cached module references --
local _GetDigest_

-- Exports --
local M = {}

--- DOCME
function M.CompareToDigest (digest, name, method, raw)
	return _GetDigest_(name, method, raw) == digest
end

--- DOCME
function M.GetDigest (name, method, raw)
	if type(method) == "string" then
		method = crypto[method]
	end

	if type(method) ~= "function" then
		method = crypto.sha512
	end

	local file = open(name, "rb")

	if file then
		local contents = file:read("*a")

		file:close()

		return crypto.digest(contents, method, raw)
	else
		return ""
	end
end

if lp_ok then
	local cpath = ("%q"):format(package.cpath)

	--- DOCME
	function M.NewLuaProc (name, body, opts)
		local opts_process_dir, cmdline

		if opts then
			if type(opts.cmdline) == "string" then
				cmdline = opts.cmdline
			end

			if opts.process_dir then
				if type(opts.process_dir) == "string" then
					opts_process_dir = '"' .. opts.process_dir .. '"'
				else
					opts_process_dir = '"@' .. system.pathForFile(nil, system.DocumentsDirectory) .. '"'
				end
			end
		end

		luaproc.newproc([[
			package.cpath = ]] .. cpath .. [[

			local x64bridge = require("plugin.x64bridge")
			local bridge = x64bridge.LaunchProcess("]] .. name .. [[", {
				cmdline = ]] .. (cmdline or [[nil]]) .. [[,
				process_dir = ]] .. (opts_process_dir or [[nil]]) .. [[
			})

			if bridge then
				]] .. body .. [[
			end
		]])
	end
end

-- Cache module members.
_GetDigest_ = M.GetDigest

-- Export the module.
return M