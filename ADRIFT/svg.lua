local simage, sw, sh = imagekit.svg.parse_from_file("nano.svg")
local smap = memoryBitmap.newTexture{ width = sw, height = sh, format = 4 }
local svg = display.newImage(smap.filename, smap.baseDir)

smap:setBytes(simage:rasterize(0, 0, 1, sw, sh))
smap:invalidate()

svg.x, svg.y = display.contentCenterX, display.contentHeight - 350