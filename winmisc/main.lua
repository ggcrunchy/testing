--- Test for winmisc plugin.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

local Bytemap = require("plugin.Bytemap")
local winmisc = require("plugin.winmisc")

local Test = 6

if Test == 1 then -- capture
	for i = 1, 2500 do
		local x, y = math.random(display.contentWidth), math.random(display.contentHeight)
		local circle = display.newCircle(x, y, math.random(2, 7))

		circle:setFillColor(math.random(), math.random(), math.random())
	end

	timer.performWithDelay(100, function()
		winmisc.CopyScreenToClipboard()
	end)
elseif Test == 2 then -- clipboard-to-bitmap
	local rgb, w, h = winmisc.GetImageDataFromClipboard()
	
	if rgb then
		print(#rgb, w, h)

		local bmap = Bytemap.newTexture{ width = w, height = h, format = "rgb" }

		bmap:SetBytes(rgb)

		local rect = display.newImageRect(bmap.filename, bmap.baseDir, display.contentWidth, display.contentHeight)
		
		rect.x, rect.y = display.contentCenterX, display.contentCenterY

	else
		print("NOPE (#2)")
	end
elseif Test == 3 then -- screen-to-image
	for i = 1, 2500 do
		local x, y = math.random(display.contentWidth), math.random(display.contentHeight)
		local rect = display.newRect(x, y, math.random(20, 70), math.random(20, 70))

		rect:setFillColor(math.random(), math.random(), math.random())
	end

	timer.performWithDelay(100, function()
		local rgb, w, h = winmisc.GetImageDataFromScreen()
	
		if rgb then
			print(#rgb, w, h)

			local bmap = Bytemap.newTexture{ width = w, height = h, format = "rgb" }

			bmap:SetBytes(rgb)

			local rect = display.newImageRect(bmap.filename, bmap.baseDir, display.contentWidth, display.contentHeight)
			
			rect.x, rect.y = display.contentCenterX, display.contentCenterY
		else
			print("NOPE (#3)")
		end
	end)
elseif Test == 4 then -- enumerate desktops
	winmisc.EnumerateDesktops(function(name)
		print("Desktop:", name)
	end)
elseif Test == 5 then -- enumerate desktop windows
	local first

	winmisc.EnumerateDesktops(function(name)
		first = name

		return "done"
	end)

	assert(first, "Didn't have any desktops!")

	winmisc.EnumerateDesktopWindows(first, function(window)
		local text = winmisc.GetWindowText(window)
			
		if #text > 0 and winmisc.IsWindowVisible(window) then
			print("WINDOW", window)
			print("Window text:", text)
		end
	end)
elseif Test == 6 then -- enumerate windows
	winmisc.EnumerateWindows(function(window)
		print("WINDOW", window)

		print("Window text:", winmisc.GetWindowText(window))
	end)
elseif Test == 7 then -- get foreground window and text
	local window = winmisc.GetForegroundWindow()

	if window then
		print("Foreground window text:", winmisc.GetWindowText(window))
	else
		print("No forgeround window (or hidden)")
	end
elseif Test == 8 then -- clipboard text
	local has_text, cur = winmisc.GetClipboardText()

	if has_text then
		print("Clipboard text:", cur)
	end

	winmisc.SetClipboardText("BLARGH")
elseif Test == 9 then -- capture window
	local index = 1
	local choice = 4 -- arbitrary window

	winmisc.EnumerateWindows(function(window)
		local text = winmisc.GetWindowText(window)

		if #text > 0 and winmisc.IsWindowVisible(window) then
			if index == choice then
				print("Window text:", text)

				winmisc.CopyWindowToClipboard(window)
				
				return "done"
			else
				index = index + 1
			end
		end
	end)
elseif Test == 10 then -- window-to-image
	local index = 1
	local choice = 6 -- arbitrary window

	winmisc.EnumerateWindows(function(window)
		local text = winmisc.GetWindowText(window)

		if #text > 0 and winmisc.IsWindowVisible(window) then
			if index == choice then
				print("Window text:", text)

				timer.performWithDelay(100, function()
					local rgb, w, h = winmisc.GetImageDataFromWindow(window)
				
					if rgb then
						print(#rgb, w, h)

						local bmap = Bytemap.newTexture{ width = w, height = h, format = "rgb" }

						bmap:SetBytes(rgb)

						local rect = display.newImageRect(bmap.filename, bmap.baseDir, display.contentWidth, display.contentHeight)
						
						rect.x, rect.y = display.contentCenterX, display.contentCenterY
					else
						print("NOPE (#3)")
					end
				end)
				
				return "done"
			else
				index = index + 1
			end
		end
	end)
end