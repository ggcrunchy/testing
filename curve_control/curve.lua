--- Curve control widget.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local abs = math.abs
local assert = assert
local floor = math.floor
local insert = table.insert
local ipairs = ipairs
local remove = table.remove
local type = type

-- Solar2D globals --
local display = display

-- Exports --
local M = {}

--
--
--

local function UpdateIndices (cgroup)
  for i, v in ipairs(cgroup.m_points) do
    v.m_index = i
  end
end

--
--
--

local function Perlin (t)
	return t^3 * (t * (t * 6 - 15) + 10)
end

local N = 15
local T1 = Perlin(1 / N)

local function UpdateCurve (cgroup, first_point_index)
  display.remove(cgroup.m_curves[first_point_index])

  local points, new_curve = cgroup.m_points
  local p1, p2 = points[first_point_index], points[first_point_index + 1]
  local x, y = p1.x, p1.y
  local dx, dy = (p2.x - x) / N, p2.y - y

  if abs(dy) > 5 then
    new_curve = display.newLine(cgroup.m_line_group, x, y, x + dx, y + dy * T1)

    for i = 2, N - 1 do
      x = x + dx

      new_curve:append(x, y + dy * Perlin(i / N))
    end

    new_curve:append(p2.x, p2.y)
  else -- curve can disappear when flat
    new_curve = display.newLine(cgroup.m_line_group, x, y, p2.x, p2.y)
  end

  new_curve:setStrokeColor(.3, .5, .2)

  new_curve.strokeWidth = 5
-- ^^^ TODO: theming
  cgroup.m_curves[first_point_index] = new_curve
end

--
--
--

local function UpdatePoint (cgroup, point)
    local points = cgroup.m_points
    local index, n = point.m_index, #points

    if index > 1 and index < n then -- between two segments
      UpdateCurve(cgroup, point.m_index - 1)
      UpdateCurve(cgroup, point.m_index)
    elseif index == 1 then -- left-hand side
      UpdateCurve(cgroup, 1)
    elseif index == n then -- right-hand side
      UpdateCurve(cgroup, n - 1)
    end
end

local function Clamp (value, low, high)
  if value < low then
    return low
  elseif value > high then
    return high
  else
    return value
  end
end

local function MovePoint (point, y)
    local cgroup = point.parent
    local box = cgroup.m_box
    local halfh = box.height / 2

    point.y = Clamp(y - point.m_offy, box.y - halfh, box.y + halfh)

    UpdatePoint(cgroup, point)
end

--
--
--

local function TouchPoint (event)
  local phase, point = event.phase, event.target

  if phase == "began" then
    point.m_offy = event.y - point.y

    display.getCurrentStage():setFocus(point)
  elseif phase == "moved" then
    MovePoint(point, event.y)
  else
    display.getCurrentStage():setFocus(nil)
  end

  return true
end

--
--
--

local function TouchPointBelow (event)
  local phase, point = event.phase, event.target

  if phase == "began" then
    display.getCurrentStage():setFocus(point)
  elseif phase == "ended" or phase == "cancelled" then
    local cgroup, other = point.parent, point.m_other
    local index = other.m_index

    cgroup.m_buckets[other.m_bucket_index] = false

    cgroup.m_curves[index]:removeSelf()

    remove(cgroup.m_curves, index)
    remove(cgroup.m_points, index)

    display.getCurrentStage():setFocus(nil)

    other:removeSelf()
    point:removeSelf()

    UpdateCurve(cgroup, index - 1) -- reconstruct curve from point[index - 1] to point[index]
    UpdateIndices(cgroup)
  end

  return true
end

--
--
--

local function GetBucketIndex (box, x)
  local left = box.contentBounds.xMin

  return floor((x - left) / box.m_bucket_size) + 1
end

--
--
--

local function AddPoint (cgroup, x, y, index)
  local point = display.newCircle(x, y, 15)

  point:addEventListener("touch", TouchPoint)
  point:setFillColor(.6, 0, 0)
  point:setStrokeColor(0, .3, 0)

  point.strokeWidth = 2

  if index then -- interior point
    insert(cgroup.m_points, index, point)
  else -- left or right
    insert(cgroup.m_points, point)
  end

  cgroup:insert(point)

  return point
end

--
--
--

local function AddPointBelow (cgroup, other)
  local below = display.newCircle(other.x, cgroup.m_offset, 12)

  below:addEventListener("touch", TouchPointBelow)
  below:setFillColor(0, .6, 0)
  below:setStrokeColor(.3, 0, 0)

  below.strokeWidth = 2

  below.m_other = other -- point to remove

  cgroup:insert(below)
end

--
--
--

local function TouchBox (event)
  local phase, box = event.phase, event.target
  local cgroup = box.parent
  local y = event.y - cgroup.y

  if phase == "began" then
    local index = GetBucketIndex(box, event.x)
    local cur = cgroup.m_buckets[index]

    if cur then -- already in use?
      box.m_current, cur.m_offy = cur, 0 -- grab the point

      MovePoint(cur, y)
    else
      local n, buckets = 0, cgroup.m_buckets

      for i = 1, index - 1 do
        if buckets[i] then 
          n = n + 1
        end
      end

      insert(cgroup.m_curves, n + 1, false) -- add stub; will be ignored by display.remove()

      local new_point = AddPoint(cgroup, cgroup.m_padding + (index - .5) * box.m_bucket_size, y, n + 1)

      AddPointBelow(cgroup, new_point)
      UpdateIndices(cgroup)
      UpdatePoint(cgroup, new_point) -- replaces stub with new segment on left, expropriates current segment on right

      cgroup.m_buckets[index] = new_point
      new_point.m_bucket_index = index
      box.m_current, new_point.m_offy = new_point, 0 -- grab the point
    end

    display.getCurrentStage():setFocus(box)
  elseif phase == "moved" then
    local current = box.m_current

    if current then
      MovePoint(current, y) -- treat a drag over the box as moving the grabbed point
    end
  elseif phase == "ended" or phase == "cancelled" then
    box.m_current = nil

    display.getCurrentStage():setFocus(nil)
  end

  return true
end

--
--
--

--- Get the current points represented by the curve control.
-- @function CurveControl:GetPoints
-- @treturn {number...} Array of values: _x1_, _y1_, _x2_, _y2_, ... with the `x` growing.
-- Values fall between 0 and 1 inclusive, according to where they were in the box.

local function GetPoints (curve)
  local box, points = curve.m_box, curve.m_points
  local w, h, results = box.width, box.height, {}
  local bx, by = box.x - w / 2, box.y - h / 2

  for i = 1, #points do
    local x, y = (points[i].x - bx) / w, (points[i].y - by) / h

    results[#results + 1] = Clamp(x, 0, 1)
    results[#results + 1] = Clamp(1 - y, 0, 1)
  end

  return results
end

--
--
--

--- Create a new curve control widget.
-- @ptable options Configuration options, which include:
--
-- * **parent**: The control will be inserted into this group. (By default, the stage.)
-- * **width**: Width of the points / curves box...
-- * **height**: ...and the height.
-- * **bucket_size**: Width of a bucket within the points / curves box.
-- * **above**: Distance from top of control to points / curves box...
-- * **below**: ...and from the bottom.
-- * **padding**: Distance from the left and right edges to the points / curves box.
-- @treturn CurveControl New control.
function M.New (options)
  assert(options == nil or type(options) == "table", "Invalid options")

  -- Configure the control's theme.
  local parent, w, h, size, above, below, padding
  
  if options then
    parent = options.parent
    w, h = options.width, options.height
    size = options.bucket_size
    above = options.above
    below = options.below
    padding = options.padding
  end

  parent = parent or display.getCurrentStage()
  w, h = w or 800, h or 150
  size = size or 45
  above = above or 20
  below = below or 40
  padding = padding or 20

  -- Prepare the control and add its methods.
  local cgroup = display.newGroup()

  cgroup.GetPoints = GetPoints

  parent:insert(cgroup)

  -- Add a general backdrop for the control.
  local back = display.newRect(cgroup, w / 2 + padding, (h + above + below) / 2, w + 2 * padding, h + above + below)

  back:setFillColor(.7)
-- ^^^ TODO: theming
  -- Add box that will hold the points and curves.
  local box = display.newRect(cgroup, back.x, above + h / 2, w, h)

  box:addEventListener("touch", TouchBox)
  box:setFillColor(.3)
  box:setStrokeColor(0, 0, .7)

  box.strokeWidth = 3
-- ^^^ TODO: theming
  box.m_bucket_size = size
  cgroup.m_box = box
  cgroup.m_offset = h + above + below / 2
  cgroup.m_padding = padding

  -- Prepare to add points and curves.
  cgroup.m_curves = {}
  cgroup.m_line_group = display.newGroup()

  cgroup:insert(cgroup.m_line_group)

  -- Put a couple points in the lower corners: (0, 0), (1, 0). Add a "curve" between them,
  -- set up the initial indices, and claim their buckets.
  cgroup.m_points = {}

  AddPoint(cgroup, padding, above + h)
  AddPoint(cgroup, padding + w, above + h)
  UpdateCurve(cgroup, 1)
  UpdateIndices(cgroup)

  local bucket2 = GetBucketIndex(box, cgroup.m_points[2].x)

  cgroup.m_buckets = { cgroup.m_points[1], [bucket2] = cgroup.m_points[2] }

  return cgroup
end

--
--
--

return M