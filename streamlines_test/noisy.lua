--- Noisy demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local cos = math.cos
local pi = math.pi
local sin = math.sin

local streamlines = require("plugin.streamlines")
local utils = require("utils")

--
--
--

local DoHandwritingVariant = true

--
--
--

graphics.defineEffect{
	category = "generator", name = "noisy",

	vertexData = {
		{ name = "variant", index = 0, default = 0, min = 0, max = 1 }
	},

	vertex = [[
		// <https:--www.shadertoy.com/view/4dS3Wd>
		// By Morgan McGuire @morgan3d, http:--graphicscodex.com
		// 
		P_UV float hash(P_POSITION float n) { return fract(sin(n) * 1e4); }
		P_UV float hash(P_POSITION vec2 p) { return fract(1e4 * sin(17.0 * p.x + p.y * 0.1) * (0.1 + abs(sin(p.y * 13.0 + p.x)))); }
		P_UV float noise(P_POSITION float x) {
			P_UV float i = floor(x);
			P_UV float f = fract(x);
			P_UV float u = f * f * (3.0 - 2.0 * f);
			return mix(hash(i), hash(i + 1.0), u);
		}

		P_POSITION vec2 VertexKernel (P_POSITION vec2 pos)
		{
			P_POSITION vec2 uv = a_ColorScale.xy; // dirty hack :)
			P_POSITION vec2 spine_to_edge = CoronaTexCoord;

			if (CoronaVertexUserData.x < 0.5)
			{
				float wave = 0.5 + 0.5 * sin(10.0 * 6.28318 * uv.x);

				pos += spine_to_edge * 2.55 * wave;
			}

			P_UV float v_along_edge = 2. * uv.y - 1.;	// convert back to [-1, +1], cf. MakeMesh()
				
			pos += v_along_edge * spine_to_edge * .85 * noise(100.0 * sin(6.28 * uv.x));

			return pos;
		}
	]],

	fragment = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			return vec4(0., 0., 0., .8);
		}
	]]
}

local SLICES = 100
local vertices = {}

local function MakeMesh (context, inputs)
	local result = context:mesh_from_lines(inputs)
	local annotations, uvs = result:GetAnnotations(), {}
	local n = result:GetNumVertices()

	for i = 1, n do
		local ma = annotations[i]

		uvs[#uvs + 1] = ma.z
		uvs[#uvs + 1] = ma.w
	end

	local mesh = display.newMesh{
		x = display.contentCenterX, y = display.contentCenterY,
		indices = result:GetTriangles(), vertices = result:GetPositions(), uvs = uvs,
		mode = "indexed", zeroBasedIndices = true
	}

	for i = 1, n do
		local ma = annotations[i]

		mesh:setFillVertexColor(i, ma.x, .5 + .5 * ma.y, 0) -- convert y to [0, 1]
	end

	mesh:translate(-100, 50)

	mesh.fill.effect = "generator.custom.noisy"

	if DoHandwritingVariant then
		mesh.fill.effect.variant = 1
	end

	return mesh
end


local context = streamlines.create_context{ thickness = 3, flags = "ANNOTATIONS" }

local inputs = {
	vertices = vertices, spine_lengths = { SLICES, SLICES }, closed = true
}

local dtheta, dx = pi * 2 / SLICES, 400 / SLICES

local function UpdateVertices (theta)
	for i = 1, SLICES do
		local j = i * 2

		vertices[j - 1] = 300 + 100 * cos(theta)
		vertices[j] = 150 + 100 * sin(theta)

		theta = theta + dtheta
	end

	local x = 100

	for i = 1, SLICES do
		local j = (SLICES + i) * 2

		vertices[j - 1] = x
		vertices[j] = 150

		x = x + dx
	end
end

local function UpdateAnnotations (mesh, annotations, n)
	local path = mesh.path

	for i = 1, n do
		local ma = annotations[i]

		path:setUV(i, ma.z, ma.w)
		mesh:setFillVertexColor(i, ma.x, .5 + .5 * ma.y, 0) -- convert y to [0, 1]
	end
end

--	UpdateVertices(0)
for i = 1, 400 do -- hack! (mesh doesn't like the initial layout)
	vertices[i] = 400 * math.random()
end

local mesh = MakeMesh(context, inputs)

mesh.isVisible = false

local start = system.getTimer()

timer.performWithDelay(50, function(event)
	UpdateVertices((event.time - start) / 1000)
	utils.UpdateMesh(context, inputs, mesh, UpdateAnnotations)

	mesh.isVisible = true -- cf. hack just above
end, 0)