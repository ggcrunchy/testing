--- Endcap demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local cos = math.cos
local pi = math.pi
local sin = math.sin

local streamlines = require("plugin.streamlines")
local utils = require("utils")

--
--
--

local WantBothCapsRound = false

--
--
--

graphics.defineEffect{
	category = "generator", name = "endcap",

	vertexData = {
		{ name = "variant", index = 0, default = 0, min = 0, max = 1 }
	},

	vertex = [[
		varying float v_Distance;
		varying float v_VAcrossCurve;

		P_POSITION vec2 VertexKernel (P_POSITION vec2 pos)
		{
			v_Distance = abs(CoronaTexCoord.x) - 1.; // cf. MakeMesh()
			v_VAcrossCurve = sign(CoronaTexCoord.x);

			return pos;
		}
	]],

	fragment = [[
		varying float v_Distance;
		varying float v_VAcrossCurve;

		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			#define radius 15.
			#define radius2 (15. * 15.)

			P_UV float dist1 = v_Distance;
			P_UV float dist2 = uv.y - dist1;
			P_UV float dist = CoronaVertexUserData.x > 0.5 ? min(dist1, dist2) : dist1;

		#if defined(GL_ES)
			P_UV float x = dist - radius;
			P_UV float y = v_VAcrossCurve * radius;
			P_UV float alpha = 1.0 - step(dist, radius) * smoothstep(radius2 - .025, radius2, x * x + y * y);
		#else
			P_UV float alpha = 1.0;

			if (dist < radius)
			{
				P_UV float x = dist - radius;
				P_UV float y = v_VAcrossCurve * radius;
				P_UV float d2 = x * x + y * y;
				P_UV float t = fwidth(d2);

				alpha = 1.0 - 0.99 * smoothstep(radius2 - t, radius2 + t, d2);
			}
		#endif

			return vec4(0., 0., 0., alpha);
		}
	]]
}

local function MakeMesh (context, inputs)
	local result = context:mesh_from_lines(inputs)
	local annotations, uvs = result:GetAnnotations(), {}
	local lengths = result:GetSpineLengths()

	for i = 1, result:GetNumVertices() do
		uvs[#uvs + 1] = (1 + annotations[i].x) * annotations[i].y -- y = -1 or +1, so ensure non-zero and encode the two
		uvs[#uvs + 1] = lengths[i]
	end

	local mesh = display.newMesh{
		x = display.contentCenterX, y = display.contentCenterY,
		indices = result:GetTriangles(), vertices = result:GetPositions(), uvs = uvs,
		mode = "indexed", zeroBasedIndices = true
	}

	mesh.fill.effect = "generator.custom.endcap"

	if WantBothCapsRound then
		mesh.fill.effect.variant = 1
	end

	mesh:scale(.3, .3)

	return mesh
end

local context = streamlines.create_context{
	thickness = 30,
	flags = { "ANNOTATIONS", "SPINE_LENGTHS" },
	u_mode = "DISTANCE"
}

local vertices = {
	50, 150, 200, 100, 550, 200,
	400, 200, 400, 100
}

local inputs = {
	vertices = vertices, spine_lengths = { 3, 2 }
}

local mesh = MakeMesh(context, inputs)
local start = system.getTimer()

timer.performWithDelay(50, function(event)
	local angle = pi * (event.time - start) / 1000

	-- v[1].y
	vertices[4] = 150 + 100 * sin(angle)

	-- v[3]
	vertices[7] = 400 + 50  * cos(angle)
	vertices[8] = 150 + 50  * sin(angle)

	-- v[4]
	vertices[9] = 400 - 50  * cos(angle)
	vertices[10] = 150 - 50  * sin(angle)

	utils.UpdateMesh(context, inputs, mesh)
end, 0)