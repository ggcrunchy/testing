--- Closed demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local cos = math.cos
local pi = math.pi
local sin = math.sin

local streamlines = require("plugin.streamlines")
local utils = require("utils")

--
--
--

local SLICES = 10
local vertices = {}

local offset = SLICES * 2

vertices[offset + 1] = 300 + 30
vertices[offset + 2] = 150

vertices[offset + 3] = 500 + 30
vertices[offset + 4] = 100

vertices[offset + 5] = 500 + 30
vertices[offset + 6] = 190

vertices[offset + 7] = 100 - 30
vertices[offset + 8] = 190

vertices[offset +  9] = 300 - 30
vertices[offset + 10] = 150

vertices[offset + 11] = 100 - 30
vertices[offset + 12] = 100

local function MakeMesh (context, inputs)
	local result = context:mesh_from_lines(inputs)

	local mesh = display.newMesh{
		x = display.contentCenterX, y = display.contentCenterY,
		indices = result:GetTriangles(), vertices = result:GetPositions(), uvs = uvs,
		mode = "indexed", zeroBasedIndices = true
	}

	mesh:scale(.3, .3)
	mesh:setFillColor(0)

	mesh.alpha = .8

	return mesh
end

local context = streamlines.create_context{ thickness = 20 }

local inputs = {
	vertices = vertices, spine_lengths = { SLICES, 3, 3 }, closed = true
}

local dtheta = pi * 2 / SLICES

local function UpdateVertices (theta)
	for i = 1, SLICES do
		local j = i * 2

		vertices[j - 1] = 300 + 100 * cos(theta)
		vertices[j] = 150 + 100 * sin(theta)

		theta = theta + dtheta
	end
end

UpdateVertices(0)

local mesh = MakeMesh(context, inputs)
local start = system.getTimer()

timer.performWithDelay(50, function(event)
	UpdateVertices((event.time - start) / 1000)
	utils.UpdateMesh(context, inputs, mesh)
end, 0)