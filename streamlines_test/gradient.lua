--- Gradient demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local streamlines = require("plugin.streamlines")

--
--
--

graphics.defineEffect{
	category = "generator", name = "gradient",

	fragment = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			P_COLOR vec3 color = mix(vec3(0.0, 0.0, 0.8), vec3(0.0, 0.8, 0.0), uv.x);

			return vec4(color, 1.);
		}
	]]
}

local function MakeMesh (context, inputs)
	local result = context:mesh_from_lines(inputs)
	local annotations, uvs = result:GetAnnotations(), {}
	local lengths = result:GetSpineLengths()

	for i = 1, result:GetNumVertices() do
		uvs[#uvs + 1] = annotations[i].x
		uvs[#uvs + 1] = 0
	end

	local mesh = display.newMesh{
		x = display.contentCenterX, y = display.contentCenterY,
		indices = result:GetTriangles(), vertices = result:GetPositions(), uvs = uvs,
		mode = "indexed", zeroBasedIndices = true
	}

	mesh.fill.effect = "generator.custom.gradient"

	mesh:scale(.3, .3)

	return mesh
end

local context = streamlines.create_context{
	thickness = 15, flags = "ANNOTATIONS"
}

MakeMesh(context, {
	vertices = {
		50, 150, 200, 100, 550, 200,
		400, 200, 400, 100
	}, spine_lengths = { 3, 2 }
})