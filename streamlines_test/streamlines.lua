--- Streamlines demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local sin = math.sin

local streamlines = require("plugin.streamlines")

--
--
--

local WantRandomOffsets = true
local UseNormalizedDistanceVariant-- = true

local WantManyVertices-- = true

--
--
--

graphics.defineEffect{
	category = "generator", name = "streamlines", isTimeDependent = true,

	vertexData = {
		{ name = "variant", index = 0, default = 0, min = 0, max = 1 }
	},

	fragment = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			P_UV float L = uv.x - CoronaTotalTime;

			if (CoronaVertexUserData.x >= 0.5) L += uv.y;

			return vec4(0., 0., 0., 0.5 * fract(L));
		}
	]]
}

local pixel_ratio = 2 / display.contentScaleY -- ???
local w, h

if WantManyVertices then
	w, h = 200, 200
else
	w, h = 100, 100
end

local right, bottom = w * pixel_ratio, h * pixel_ratio

local friction = 1.0
local g = 1.0
local L = 1.0
local dt = 2.0
local initial_frames = 0
local frame_count = 100

local function Advect (px, py)
	local x = 8.0 * (px / right - 0.5)
	local y = 4.0 * (py / bottom - 0.5)
	local theta = x
	local omega = y
	local omega_dot = -friction * omega - g / L * sin(theta)

	px = px + dt * omega
	py = py + dt * omega_dot

	return px, py
end

local function MakeMesh (context)
	local result = context:mesh_from_streamlines(Advect, initial_frames, frame_count)
	local n, annotations, uvs, offsets = result:GetNumVertices(), result:GetAnnotations(), {}

	if WantRandomOffsets then
		offsets = result:GetRandomOffsets()
	end

	local indices, vertices = result:GetTriangles(), result:GetPositions()

	if n > 64 * 1024 then
		local from = vertices

		vertices = {}

		for i = 1, result:GetNumTriangles() * 3 do
			local index = indices[i]
			local offset = index * 2

			vertices[#vertices + 1] = from[offset + 1]
			vertices[#vertices + 1] = from[offset + 2]

			uvs[#uvs + 1] = annotations[index + 1].x
			uvs[#uvs + 1] = WantRandomOffsets and offsets[index + 1] or 0
		end

		indices = nil
	else
		for i = 1, n do
			uvs[#uvs + 1] = annotations[i].x
			uvs[#uvs + 1] = WantRandomOffsets and offsets[i] or 0
		end
	end

	local mesh = display.newMesh{
		x = display.contentCenterX, y = display.contentCenterY,
		indices = indices, vertices = vertices, uvs = uvs,
		mode = indices and "indexed" or "triangles", zeroBasedIndices = true
	}

	mesh:setFillColor(0)

	mesh.fill.effect = "generator.custom.streamlines"

	if WantRandomOffsets then
		mesh.fill.effect.variant = 1
	end

	return mesh
end

local margin

if WantManyVertices then
	margin = 50 * pixel_ratio
else
	margin = 20 * pixel_ratio
end

local flags = { "ANNOTATIONS" }

if WantRandomOffsets then
	flags[#flags + 1] = "RANDOM_OFFSETS"
end

local context = streamlines.create_context{
	thickness = 3, flags = flags,
	u_mode = UseNormalizedDistanceVariant and "NORMALIZED_DISTANCE" or "SEGMENT_FRACTION",
	streamlines_seed_spacing = 40,
	streamlines_seed_viewport = {
		left = -margin, top = -margin,
		right = right + margin,
		bottom = bottom + margin
	}
}

MakeMesh(context)