--- Wireframe demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local abs = math.abs
local cos = math.cos
local pi = math.pi
local sin = math.sin

local streamlines = require("plugin.streamlines")

--
--
--

local SLICES = 16
local vertices = {}

local function UpdateVertices (t)
	for i = 0, SLICES - 1 do
		local theta = i * pi * 2 / SLICES
		local lx, ly = 20 + 430 * i / SLICES, 100
		local angle = theta - t * pi / 4
		local cx, cy = 400 + 75 * cos(angle), 150 + 65 * sin(angle)
		local t = 2 * abs((t % 2) / 2 - .5)

		t = easing.inOutCubic(t, 1, 0, 1)

		local offset = i * 2

		vertices[offset + 1] = lx * t + cx * (1 - t)
		vertices[offset + 2] = ly * t + cy * (1 - t)
	end

	local angle = pi * t
	
	vertices[SLICES * 2 + 1] = 400 + 50 * cos(angle)
	vertices[SLICES * 2 + 2] = 150 + 50 * sin(angle)
	vertices[SLICES * 2 + 3] = 400
	vertices[SLICES * 2 + 4] = 150
	vertices[SLICES * 2 + 5] = 400 - 50 * cos(angle)
	vertices[SLICES * 2 + 6] = 150 - 50 * sin(angle)
end

local lines

local function UpdateWireframe (context, inputs)
	display.remove(lines)

	local result = context:mesh_from_lines(inputs)
	local positions, triangles = result:GetPositions(), result:GetTriangles()

	lines = display.newGroup()

	local cx, cy = display.contentCenterX - 200, display.contentCenterY + 100

	for i = 1, result:GetNumTriangles() * 4, 4 do
		local i1, i2, i3 = triangles[i] * 2, triangles[i + 1] * 2, triangles[i + 2] * 2 -- i1 == i4
		local x1, y1 = cx + positions[i1 + 1], cy - positions[i1 + 2]
		local x2, y2 = cx + positions[i2 + 1], cy - positions[i2 + 2]
		local x3, y3 = cx + positions[i3 + 1], cy - positions[i3 + 2]

		local line = display.newLine(lines, x1, y1, x2, y2, x3, y3, x1, y1)

		line:setStrokeColor(0)

		line.strokeWidth = 2
	end
end

local context = streamlines.create_context{ thickness = 15, flags = "WIREFRAME", miter_limit = 15 }

local inputs = {
	vertices = vertices, spine_lengths = { SLICES, 3 }
}

local start = system.getTimer()

timer.performWithDelay(50, function(event)
	UpdateVertices((event.time - start) / 1000)
	UpdateWireframe(context, inputs)
end, 0)