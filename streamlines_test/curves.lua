--- Curves demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local pi = math.pi
local sin = math.sin

local streamlines = require("plugin.streamlines")

--
--
--

local WantCubicVariant = true
local WantCubicTest = true

--
--
--

local lengths, vertices

if WantCubicVariant then
	lengths = { 6 }
	vertices = {
		10, 80,   -- P
		40, 10,   -- C1
		65, 10,   -- C2
		95, 80,   -- P
		150, 150, -- C2
		180, 80,  -- P
	}
else
	lengths = { 3 }
	vertices = {
		10, 80,  -- P
		95, 10,  -- C
		180, 80 -- P
	}

	WantCubicTest = false
end

local inputs = { spine_lengths = lengths, vertices = vertices }
local params = {} -- n.b. the demo also requests annotations, then never actually uses them

if WantCubicTest then
	params.flags = "WIREFRAME"
	params.max_curve_flatness = 5
	params.thickness = 15

else
	params.flags = "CURVE_GUIDES"
	params.max_curve_flatness = .5
	params.thickness = 5
end

local context = streamlines.create_context(params)

local function UpdateWireframe (group, result)
	local positions, triangles = result:GetPositions(), result:GetTriangles()
	local cx, cy = display.contentCenterX - 200, display.contentCenterY + 100

	for i = 1, result:GetNumTriangles() * 4, 4 do
		local i1, i2, i3 = triangles[i] * 2, triangles[i + 1] * 2, triangles[i + 2] * 2 -- i1 == i4
		local x1, y1 = cx + positions[i1 + 1], cy - positions[i1 + 2]
		local x2, y2 = cx + positions[i2 + 1], cy - positions[i2 + 2]
		local x3, y3 = cx + positions[i3 + 1], cy - positions[i3 + 2]

		local line = display.newLine(group, x1, y1, x2, y2, x3, y3, x1, y1)

		line:setStrokeColor(0)

		line.strokeWidth = 2
	end
end

local function UpdateVertices (context, t)
	local angle = pi * t

	if WantCubicVariant then
		local coeff = WantCubicTest and .125 or 20

		-- v[2]
		vertices[5] = 65 + coeff * sin(angle)
		vertices[6] = 10

		return context:mesh_from_curves_cubic(inputs)
	else
		-- v[1]
		vertices[3] = 95 + 20 * sin(angle)
		vertices[4] = 20 + 20 * sin(angle)

		return context:mesh_from_curves_quadratic(inputs)
	end
end

local group
local start = system.getTimer()

timer.performWithDelay(50, function(event)
	display.remove(group)

	group = display.newGroup()

	group.alpha = .75

	local result = UpdateVertices(context, (event.time - start) / 1000)

	if WantCubicTest then
		UpdateWireframe(group, result)
	else
		local mesh = display.newMesh{
			parent = group,
			x = display.contentCenterX, y = display.contentCenterY,
			indices = result:GetTriangles(), vertices = result:GetPositions(), uvs = uvs,
			mode = "indexed", zeroBasedIndices = true
		}

		mesh:setFillColor(0)
	end
end, 0)