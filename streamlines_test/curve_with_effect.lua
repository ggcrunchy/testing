--- Curve-with-effect demo, for streamlines.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local pi = math.pi
local sin = math.sin

local streamlines = require("plugin.streamlines")

--
--
--

local WantLongCurve = true

--
--
--

graphics.defineEffect{
	-- adapted from the flame effect here: https://forums.solar2d.com/t/share-your-shaders/333202/15

	category = "generator", name = "fire",

	fragment = [[
		P_DEFAULT vec2 hash( P_DEFAULT vec2 p )
		{
			p = vec2( dot(p,vec2(127.1,311.7)),
					 dot(p,vec2(269.5,183.3)) );
			return -1.0 + 2.0*fract(sin(p)*43758.5453123);
		}

		P_DEFAULT float noise( in P_DEFAULT vec2 p )
		{
			const P_DEFAULT float K1 = 0.366025404; // (sqrt(3)-1)/2;
			const P_DEFAULT float K2 = 0.211324865; // (3-sqrt(3))/6;
			P_DEFAULT vec2 i = floor( p + (p.x+p.y)*K1 );
			P_DEFAULT vec2 a = p - i + (i.x+i.y)*K2;
			P_DEFAULT vec2 o = (a.x>a.y) ? vec2(1.0,0.0) : vec2(0.0,1.0);
			P_DEFAULT vec2 b = a - o + K2;
			P_DEFAULT vec2 c = a - 1.0 + 2.0*K2;
			P_DEFAULT vec3 h = max( 0.5-vec3(dot(a,a), dot(b,b), dot(c,c) ), 0.0 );
			P_DEFAULT vec3 n = h*h*h*h*vec3( dot(a,hash(i+0.0)), dot(b,hash(i+o)), dot(c,hash(i+1.0)));
			return dot( n, vec3(70.0) );
		}

		P_DEFAULT float fbm(P_DEFAULT vec2 uv)
		{
			P_DEFAULT float f;
			P_DEFAULT mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
			f  = 0.5000*noise( uv ); uv = m*uv;
			f += 0.2500*noise( uv ); uv = m*uv;
			f += 0.1250*noise( uv ); uv = m*uv;
			f += 0.0625*noise( uv ); uv = m*uv;
			f = 0.5 + 0.5*f;
			return f;
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 uv )
		{
			P_UV vec2 q = uv;

			P_DEFAULT float strength = 1.25;
			P_DEFAULT float T3 = max(3.,1.25*strength)*sin(CoronaTotalTime / .75);
			q.x = mod(q.x,1.)-0.5;
			q.y -= 0.25;
			P_DEFAULT float n = fbm(strength*q - vec2(0,T3));
			P_DEFAULT float c = 1. - 16. * pow( max( 0., length(q*vec2(q.y*1.5,.75) ) - n * max( 0., q.y+.25 ) ),1.2 );

			P_DEFAULT float c1 = n * c * (1.5-pow(.150*uv.y + fbm(uv.yx),4.));
			c1=clamp(c1,0.,1.);
			P_COLOR vec3 col = vec3(1.5*c1, 1.5*c1*c1*c1, c1*c1*c1*c1*c1*c1);

			P_DEFAULT float a = smoothstep(0., .375, c * (1.-pow(uv.y,3.)));
			return CoronaColorScale(vec4( col * a, a));
		}
	]]
}

--
--
--

local lengths, vertices = { WantLongCurve and 10 or 6 }, {
	10, 80,   -- P
	40, 10,   -- C1
	65, 10,   -- C2
	95, 80,   -- P
	150, 150, -- C2
	180, 80,  -- P
	70, 110, -- C2
	280, 80,  -- P
	130, 20, -- C2
	-40, -40, -- P
}

local inputs = { spine_lengths = lengths, vertices = vertices }
local params = { max_curve_flatness = 5, thickness = 15, flags = "ANNOTATIONS" }

local context = streamlines.create_context(params)

local function UpdateVertices (context, t)
	local angle = pi * t

	-- v[2]
	vertices[5] = 65 + 20 * sin(angle)
	vertices[6] = 10

	-- v[6]
	vertices[13] = 70 + 30 * sin(angle)
	vertices[14] = 110

	-- v[8]
	vertices[17] = 130
	vertices[18] = 20 + 70 * sin(angle)

	return context:mesh_from_curves_cubic(inputs)
end

local group
local start = system.getTimer()

timer.performWithDelay(50, function(event)
	display.remove(group)

	group = display.newGroup()

	group.alpha = .75

	local result = UpdateVertices(context, (event.time - start) / 1000)
	local annotations, uvs = result:GetAnnotations(), {}

	for i = 1, result:GetNumVertices() do
		local ma = annotations[i]

		uvs[#uvs + 1] = .5 * ma.y + .5
		uvs[#uvs + 1] = ma.x
	end

	local mesh = display.newMesh{
		parent = group,
		x = display.contentCenterX, y = display.contentCenterY,
		indices = result:GetTriangles(), vertices = result:GetPositions(), uvs = uvs,
		mode = "indexed", zeroBasedIndices = true
	}

	mesh.fill.effect = "generator.custom.fire"
end, 0)