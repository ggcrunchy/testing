--- Test for Awesomium.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]


-- Plugins --
local awesomium = require("plugin.awesomiumc")

local wv = awesomium.newWebView{ width = display.contentWidth, height = display.contentHeight }

wv:LoadURL("https://maps.google.com")

local rect = display.newImage(wv.filename, wv.baseDir)

rect.x, rect.y = display.contentCenterX, display.contentCenterY

local function GetXY (event)
	local tbounds, x, y = event.target.contentBounds, event.x, event.y

	x = math.max(tbounds.xMin, math.min(x, tbounds.xMax))
	y = math.max(tbounds.yMin, math.min(y, tbounds.yMax))

	return x, y
end

rect:addEventListener("touch", function(event)
	local phase = event.phase

	if phase == "began" then
		wv:InjectMouseMove(GetXY(event))
		wv:InjectMouseDown("left")
	elseif phase == "moved" then
		wv:InjectMouseMove(GetXY(event))
	elseif phase == "ended" or phase == "cancelled" then
		wv:InjectMouseUp("left")
	end
	
	return true
end)

timer.performWithDelay(1000, function()
	-- TODO: probably some of that IsLoading()-related stuff...
timer.performWithDelay(40, function()
	wv:update()
	wv:invalidate()
end, 0)
end)