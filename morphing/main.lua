--- Test for morphing plugin.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local open = io.open
local print = print

-- Modules --
local skeleton = require("skeleton")

-- Plugins --
local morphing = require("plugin.morphing")

-- Corona modules --
local json = require("json")
local widget = require("widget")

-- Corona globals --
local native = native
local system = system

--
skeleton.Init(true)

--
local CX, CY, W, H = skeleton.GetRegion()
local Filename = native.newTextField(CX, CY + H / 2 + 50, W, 50)

--
local function Close (file)
	if file then
		file:close()
	end
end

--
local function Exists (filename)
	local file = open(filename, "r")

	if file then
		file:close()
	end

	return file ~= nil
end

--
local Props = { indent = true }

local function Write (file, t)
	if file then
		file:write(json.encode(t, Props))
	end
end

--
widget.setTheme("widget_theme_android_holo_light")

local save = widget.newButton{
	left = CX - W / 2, top = Filename.y + 30, width = W * 3 / 7, label = "Save",

	onEvent = function(event)
		local name = Filename.text

		if event.phase == "ended" and #name > 0 then
			--
			local idata = {}

			for i = 1, skeleton.GetInteriorCount() do
				idata[i] = skeleton.GetInteriorCoords(i):GetData()
			end

			--
			local file = open(system.pathForFile(name .. ".json", system.DocumentsDirectory), "w")
			local gname = system.pathForFile(skeleton.GetGridName() .. ".json", system.DocumentsDirectory)
			local exists = Exists(gname)
			local gfile = not exists and open(gname, "w")

			if file and (exists or gfile) then
				Write(gfile, idata)
				Write(file, skeleton.GetPoissonState():GetData())
			end

			Close(file)
			Close(gfile)
		end
	end
}

--
local function Contents (name)
	local file, err = open(system.pathForFile(name .. ".json", system.DocumentsDirectory), "r")

	if file then
		local contents = json.decode(file:read("*a"))

		file:close()

		return contents
	else
		return nil, err
	end
end

widget.newButton{
	left = CX + W / 2 - save.width, top = save.y - save.height / 2, width = save.width, label = "Load",

	onEvent = function(event)
		local name = Filename.text

		if event.phase == "ended" and #name > 0 then
			local pstate, perr = Contents(name)
			local interior, ierr = Contents(skeleton.GetGridName())

			if pstate and interior then
				--
				for i = 1, skeleton.GetInteriorCount() do
					skeleton.UpdateInteriorCoords(i, morphing.PoissonCoordsFromData(interior[i]))
				end

				skeleton.UpdatePoissonState(morphing.PoissonStateFromData(pstate))

				--
				for i = 1, skeleton.GetPointCount() do
					local pos = pstate[i]

					skeleton.LoadPoint(i, pos[1], pos[2])
				end
			else
				if perr then
					print("Error opening pstate data file", perr)
				end

				if ierr then
					print("Error opening interior data file", ierr)
				end
			end
		end
	end
}

-- display.setDrawMode("wireframe")