local old = display.getDefault("textureWrapY")

display.setDefault("textureWrapY", "repeat")

local image = display.newImageRect("Image1.jpg", display.contentWidth, .4 * display.contentHeight) -- your image here (ideally one that tiles vertically)
                                                                                                   -- can shrink these dimensions to see what's going on

image.x, image.y = display.contentCenterX, display.contentCenterY + .3 * display.contentHeight

display.setDefault("textureWrapY", old)

-- --
do
	local kernel = { category = "filter", group = "custom", name = "roll" }

	kernel.vertexData = {
		{
			index = 0, name = "to_angle", min = -90, max = 80, default = 40 -- cylinder angle (starting from 90 at top) of bottom of rect
		}, {
			index = 1, name = "t", min = 0, default = 0 -- rotation (0-1, periodic)
		}
	}

	kernel.fragment = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			P_UV float yf = cos(radians(90. - CoronaVertexUserData.x));
			P_UV float y = mix(1., yf, uv.y);
			P_UV float angle = 1. - acos(y) / 3.14159 + CoronaVertexUserData.y;

			return CoronaColorScale(texture2D(CoronaSampler0, vec2(uv.x, angle)));
		}
	]]

	graphics.defineEffect(kernel)
end

image.path.x2 = -110
image.path.x3 = 110

image.fill.effect = "filter.custom.roll"

local Period = 19000

Runtime:addEventListener("enterFrame", function(event)
	image.fill.effect.t = (event.time % Period) / Period
end)