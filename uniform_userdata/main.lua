--- Test for mat3 and uniform userdata in GL and Vulkan.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

local Test = 2

if Test == 1 then
	local kernel = { category = "generator", name = "mat3" }

	kernel.uniformData = {
		{
			name = "grays",
			type = "mat3",
			index = 0, -- u_UserData0
		}
	}

	kernel.fragment = [[
		uniform P_COLOR mat3 u_UserData0;

		P_COLOR vec4 FragmentKernel (P_UV vec2 texCoord)
		{
			P_COLOR float gray;
			
			if (texCoord.y < .33)
			{
				if (texCoord.x < .33) gray = u_UserData0[0][0];
				else if (texCoord.x < .66) gray = u_UserData0[1][0];
				else gray = u_UserData0[2][0];
			}
			
			else if (texCoord.y < .66)
			{
				if (texCoord.x < .33) gray = u_UserData0[0][1];
				else if (texCoord.x < .66) gray = u_UserData0[1][1];
				else gray = u_UserData0[2][1];
			}
			
			else
			{
				if (texCoord.x < .33) gray = u_UserData0[0][2];
				else if (texCoord.x < .66) gray = u_UserData0[1][2];
				else gray = u_UserData0[2][2];
			}
			
			return vec4(vec3(gray), 1.);
		}
	]]

	graphics.defineEffect(kernel)

	local r = display.newRect(display.contentCenterX, display.contentCenterY, 350, 350)

	r.fill.effect = "generator.custom.mat3"

	r.fill.effect.grays = {
		.3, 0, .2,
		.7, .1, .4,
		1, .4, .5
	}

elseif Test == 2 then
	local kernel = { category = "generator", name = "all_mat4s" }

	kernel.uniformData = {
		{
			name = "A",
			type = "mat4",
			index = 0, -- u_UserData0
		},
		{
			name = "B",
			type = "mat4",
			index = 1, -- u_UserData1
		},
		{
			name = "C",
			type = "mat4",
			index = 2, -- u_UserData2
		},
		{
			name = "D",
			type = "mat4",
			index = 3 -- u_UserData3
		}
	}

	kernel.fragment = [[
		uniform P_UV mat4 u_UserData0, u_UserData1;
		uniform P_UV mat4 u_UserData2	// we want to ignore
										// these two lines
										;
		uniform P_UV mat4 /* this is ignored */ u_UserData3;

		P_COLOR vec4 FragmentKernel (P_UV vec2 texCoord)
		{
			P_UV vec4 result = u_UserData0 * texCoord.xyxy + u_UserData1 * texCoord.yxyx + u_UserData2 * texCoord.xyyx + u_UserData3 * texCoord.yxxy;
			
			return vec4(fract(result));
		}
	]]

	graphics.defineEffect(kernel)

	local r = display.newRect(display.contentCenterX, display.contentCenterY, 350, 350)

	r.fill.effect = "generator.custom.all_mat4s"

	r.fill.effect.A = {
		1, 0, .3, 0,
		.2, 0, 0, .3,
		.8, 0, 1, 1,
		.2, .4, .3, 1
	}
	
	r.fill.effect.B = {
		.6, .3, .2, .8,
		.9, .25, 0, .1,
		.4, .7, .3, .2,
		.9, .2, .8, 1
	}
	
	r.fill.effect.C = {
		0, 0, 1, 0,
		.4, 0, 0, .1,
		0, .3, 0, 0,
		.2, 0, .5, 0
	}

	r.fill.effect.D = {
		1, 0, .8, .2,
		.1, 0, .2, 0,
		0, 1, 0, 1,
		.2, .3, .4, 0
	}
end