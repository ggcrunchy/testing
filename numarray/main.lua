--- Test for numarray.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

local numarray = require("plugin.numarray")

local arr = numarray.NewArray("sint32", 22)

print(arr:GetType())

arr:Set(12, 3747343)
arr:Set(13, 1030300103) -- adjacent
arr:Set(17, -8338)
arr:Set(19, 2^33 - 6) -- larger than 32 bits

print("ARR")

for i = 1, #arr do
	print(i, arr:Get(i))
end

print("")

local na_math = numarray.math

local ca = na_math.cos(arr)

print("COS")

for i = 1, #ca do
	print(i, ca:Get(i))
end

print("")
print("SIN")

local sa = na_math.sin(arr)

for i = 1, #sa do
	print(i, sa:Get(i))
end

print("")
print("SUM")

local sum = ca + sa

for i = 1, #sum do
	print(i, sum:Get(i))
end

print("")