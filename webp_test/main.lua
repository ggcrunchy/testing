--- webp test.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Plugins --
local AssetReader

if system.getInfo("platform") == "android" and system.getInfo("environment") == "device" then
	AssetReader = require("plugin.AssetReader")
end

local bytemap = require("plugin.Bytemap")
local webp = require("plugin.webp")

--
--
--

display.newRect(display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight):setFillColor(.7, 0, .7)

local Files = {
	"1.webp", "1_webp_a.webp", "1_webp_ll.webp",
	"2.webp", "2_webp_a.webp", "2_webp_ll.webp",
	"3.webp", "3_webp_a.webp", "3_webp_ll.webp",
	"4.webp", "4_webp_a.webp", "4_webp_ll.webp",
	"5.webp", "5_webp_a.webp", "5_webp_ll.webp",
	"file_example_WEBP_50kB.webp", "file_example_WEBP_250kB.webp", "file_example_WEBP_500kB.webp", "file_example_WEBP_1500kB.webp"
}

---[[
for _, v in ipairs(Files) do
	local f = AssetReader or io.open(system.pathForFile(v), "rb")

	if f then
		local contents

		if AssetReader then
			contents = AssetReader.Read(v) or ""
		else
			contents = f:read("*a")
		end

		print("Info", v, #contents, webp.GetInfoFromMemory(contents))
if not AssetReader then
		f:close()
end
	else
		print("Unable to open " .. v)
	end
end
--]]
bytemap.addLoader(webp.GetLoader())

local ff = io.open(system.pathForFile(Files[5]), "rb")

local contents = ff:read("*a")

ff:close()

local format, image = "rgba" -- mask, rgb, or rgba
local bmap = bytemap.loadTexture{ format = format, from_memory = contents }--filename = Files[5] } -- 1 through 19

if format ~= "mask" then
	image = display.newImage(bmap.filename, bmap.baseDir)
else
	image = display.newRect(0, 0, display.contentWidth, display.contentHeight)

	image:setFillColor(0, 1, 0)
	image:setMask(graphics.newMask(bmap.filename, bmap.baseDir))
end

bmap:invalidate()

image.x, image.y = display.contentCenterX, display.contentCenterY