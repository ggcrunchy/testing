--- Test for graphics.undefineEffect().

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

graphics.defineEffect{
  category = "filter", name = "effect1",
  isTimeDependent = true,

  fragment = [[
    P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
    {
      return vec4(uv.x * .5 + sin(CoronaTotalTime * 3.7) * .5, 0.0, uv.y * fract(CoronaTotalTime * 18.), 1.0);
    }
  ]]
}

local r1 = display.newRect(display.contentCenterX, display.contentCenterY, 250, 250)

r1.fill.effect = "filter.custom.effect1"

for k, v in pairs(graphics.listEffects("generator")) do
--  print("GENERATOR, STEP #1: ", k, v)
end

print("")

graphics.defineEffect{
  category = "filter", name = "quantized",

  fragment = [[
    P_COLOR vec4 FragmentKernel (P_UV vec2 texCoord)
    {
      texCoord = floor(4. * texCoord) / 4.;
      
      return texture2D(CoronaSampler0, texCoord);
    }
  ]]
}

graphics.defineEffect{
  category = "filter", name = "blurred",

  graph = {
	   nodes = {
		  blur = { effect = "filter.custom.quantized", input1 = "effect1" },
		  effect1 = { effect = "filter.custom.effect1", input1 = "paint1" },
	   }, output = "blur"
   }
}

graphics.undefineEffect("filter.custom.effect1")

for k, v in pairs(graphics.listEffects("generator")) do
--  print("GENERATOR, STEP #2: ", k, v)
end

print("")

graphics.defineEffect{
  category = "filter", name = "effect1",

  fragment = [[
    P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
    {
      return vec4(uv, 0.0, 1.0);
    }
  ]]
}

local r2 = display.newRect(250, 200, 300, 300)

r2.fill.effect = "filter.custom.effect1"

for k, v in pairs(graphics.listEffects("generator")) do
--  print("GENERATOR, STEP #3: ", k, v)
end

timer.performWithDelay(100, function()
  r2.x = math.random(249, 251)
end, 0)

print("")

timer.performWithDelay(3000, function()
  r1:removeSelf()
end)

local DummyFill = { type = "image", filename = "Image1.jpg" } -- to get uvs

timer.performWithDelay(700, function()
  local x, y = math.random(display.contentWidth), math.random(display.contentHeight)
  local rect = display.newRoundedRect(x, y, 250, 250, 12)

  rect.fill = DummyFill
  rect.fill.effect = "filter.custom.blurred"
end, 5)