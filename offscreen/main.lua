--- Test for mat3 and uniform userdata in GL and Vulkan.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

local Test = 2

if Test == 1 then
	local r = display.newSnapshot(300, 300)
	
	r.x, r.y = display.contentCenterX, display.contentCenterY
	
	for _ = 1, 80 do
		local x, y, radius = math.random(-125, 125), math.random(-125, 125), math.random(15, 40)
		local c = display.newCircle(x, y, radius)
		
		c:setFillColor(math.random(), math.random(), math.random())
	
		r.group:insert(c)
	end

	r:setStrokeColor(1, 0, 0)
	
	r.strokeWidth = 2
	
	r.fill.effect = "filter.wobble"

elseif Test == 2 then

	local tex = graphics.newTexture( { type="canvas", width=128, height=128 } )
	 
	-- Create display object with texture as contents
	local rect = display.newImageRect(
		tex.filename,  -- "filename" property required
		tex.baseDir,   -- "baseDir" property required
		display.contentWidth,
		display.contentHeight
	)
	rect.x = display.contentCenterX
	rect.y = display.contentCenterY
	 
	-- Set background color to be applied if texture is cleared
	tex:setBackground( 0, 0, 1 )
	 
	-- Create a circle and draw/render it to the texture
	local circ = display.newCircle( 0, 0, 64 )
	circ:setFillColor( { type="gradient", color1={0,0.2,1}, color2={0.8,0.8,0.8}, direction="down" } )
	tex:draw( circ )
	 
	-- Create a polygon and draw/render it to the texture
	local poly = display.newPolygon( 0, 0, {0,-55,14,-18,52,-18,22,8,32,45,0,22,-32,45,-22,8,-52,-18,-14,-18} )
	poly:setFillColor( 0.2, 1, 0.2 )
	tex:draw( poly )
	 
	-- Schedule texture objects to be rendered to texture before next frame
	tex:invalidate()

end