-- config.lua

application =
{
	backend = "wantVulkan",
	content =
	{
		width = 320,
		height = 480,
		scale = "letterbox" 
	},
}