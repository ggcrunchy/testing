-- Standard library imports --
local concat = table.concat
local open = io.open

-- Plugins --
local bit = require("plugin.bit")
local bytemap = require("plugin.Bytemap")
local msquares = require("plugin.msquares")

-- Corona globals --
local system = system

--
--
--

local function Load (filename, format)
	local bmap = bytemap.loadTexture{ filename = filename, is_non_external = true, format = format }

	return bmap:GetBytes(), bmap.width, bmap.height
end

local CELLSIZE = 32
local IMGWIDTH = 1024
local IMGHEIGHT = 1024
local THRESHOLD = 0
local OCEAN_COLOR = 0x214562

local function ToStringOBJ (mlist)
	local buf, offset = {}, 0

	for i = 1, #mlist do
		local mesh = mlist:GetMesh(i)
		local points, indices, dim = mesh:GetPoints(), mesh:GetTriangles(), mesh:GetDim()
		local no_z = mesh:GetDim() == 2 and 0 or false

		for j = 1, #points, dim do -- iterates mesh:GetNumPoints() times
			buf[#buf + 1] = ("v %f %f %f"):format(points[j], points[j + 1], no_z or points[j + 2])
		end

		for j = 1, #indices, 3 do -- iterates mesh:GetNumTriangles() times
			local a, b, c = offset + indices[j], offset + indices[j + 1], offset + indices[j + 2]

			buf[#buf + 1] = ("f %d/%d %d/%d %d/%d"):format(a, a, b, b, c, c)
		end

		offset = offset + mesh:GetNumPoints()
	end

	return concat(buf, "\n")
end

local BeginSVG = [[
<svg viewBox='-.1 -.1 1.2 1.2' width='500px' height='500px' version='1.1' xmlns='http://www.w3.org/2000/svg'>
<g transform='translate(0 1) scale(1 -1)'>]]

local function ResetSVG (state)
	local mdata = state.mesh_data

	mdata.color, mdata.mask = 0xFFFFFFFF, 0xFFFFFFFF
	mdata.fill, mdata.stroke = 1, 1

	state.found_color = false
end

local function NewStateSVG ()
	local state = { buffer = { BeginSVG }, mesh_data = {} }

	ResetSVG(state)

	return state
end

local function EmitSVG (state)
	local buf = state.buffer

	buf[#buf + 1] = "</g>\n</svg>"

	local str = concat(buf, "")

	state.buffer = {}

	return str
end

local function ReadOpts (state, opts, reset)
	if reset then
		ResetSVG(state)
	end

	local mdata = state.mesh_data

	mdata.fill = opts.fill or mdata.fill
	mdata.stroke = opts.stroke or mdata.stroke
	mdata.color = opts.color or mdata.color
	mdata.mask = opts.mask or mdata.mask

	state.found_color = not not opts.color
end

local function AuxWritePathSVG (buf, mesh, color, fill, stroke)
	local polygon, cbuf = mesh:GetBoundary(), ("%6.6x"):format(color)

	buf[#buf + 1] = ([[
<path
 fill='#%s'
 stroke='#%s'
 stroke-width='0.005'
 fill-opacity='%f'
 stroke-opacity='%f'
 d=']]):format(cbuf, cbuf, fill, stroke)

	local dim = mesh:GetDim()
 
	for i = 1, #polygon do
		local chain = polygon[i]

		for j = 1, #chain, dim do
			buf[#buf + 1] = ("%s%f,%f"):format(j == 1 and "M" or "L", chain[j], chain[j + 1])
		end

		buf[#buf + 1] = "Z"
	end

	buf[#buf + 1] = "'\n/>"
end

local function WritePath (state, mesh, mstate)
	local mdata = state.mesh_data

	if mstate then
		mdata.fill, mdata.stroke, mdata.color, mdata.mask, mdata.mesh = mstate.fill, mstate.stroke, mstate.color, mstate.mask, mstate.mesh
	end

	AuxWritePathSVG(state.buffer, mesh, state.found_color and mdata.color or bit.band(mesh:GetColor(), mdata.mask), mdata.fill, mdata.stroke)
end

local function ToStringSVG (mlist, opts)
	local state = NewStateSVG()

	if opts then
		ReadOpts(state, opts, false)
	end

	for i = 1, #mlist do
		WritePath(state, mlist:GetMesh(i))
	end

	return EmitSVG(state)
end

local function WriteFile (filename, mlist, to_string, opts)
	local file = open(system.pathForFile(filename, system.DocumentsDirectory), "w")

	if file then
		file:write(to_string(mlist, opts))
		file:close()
	end
end

local function WriteOBJ (filename, mlist)
	WriteFile(filename, mlist, ToStringOBJ)
end

local function WriteSVG (filename, opts, mlist)
	if not mlist then
		mlist = opts
	end

	WriteFile(filename, mlist, ToStringSVG, opts)
end
	
local function test_multi ()
	do
		local pixels, w, h = Load("tverts.png", "rgb")
		local mlist = msquares.color_multi(pixels, w, h, 12, 3, "CLEAN")

		WriteOBJ("msquares_multi_tverts.obj", mlist)
		WriteSVG("msquares_multi_tverts.svg", {
			fill = .5, stroke = .5, mask = 0xFFFFFF
		}, mlist)
	end

	do
		local pixels, w, h = Load("rgb.png", "rgb")
		local mlist = msquares.color_multi(pixels, w, h, CELLSIZE / 2, 3, "CLEAN")

		WriteOBJ("msquares_multi_rgb.obj", mlist)
		WriteSVG("msquares_multi_rgb.svg", {
			fill = .5, stroke = .5
		}, mlist)
	end

	do
		local pixels, w, h = Load("rgba.png", "rgba")
		local mlist = msquares.color_multi(pixels, w, h, CELLSIZE / 2, 4, {
			"HEIGHTS", "CONNECT", "CLEAN"
		})

		WriteOBJ("msquares_multi_rgba.obj", mlist)
		WriteSVG("msquares_multi_rgba.svg", {
			fill = .5, stroke = .5, mask = 0xFFFFFF
		}, mlist)
	end

	do
		local pixels, w, h = Load("msquares_color.png", "rgba")
		local mlist = msquares.color_multi(pixels, w, h, CELLSIZE / 2, 4, {
			"HEIGHTS", "CONNECT", "SIMPLIFY"
		})

		WriteOBJ("msquares_multi_diagram.obj", mlist)
	end
end

local function test_color ()
	local pixels, w, h = Load("msquares_color.png", "rgba")

	assert(w == IMGWIDTH)
	assert(h == IMGHEIGHT)

    -- -----------------------------
    -- msquares_color_default
    -- -----------------------------
	do
		local mlist = msquares.color(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, OCEAN_COLOR, 4)

		WriteOBJ("msquares_color_default.obj", mlist)
	end

    -- -----------------------------
    -- msquares_color_invert_heights
    -- -----------------------------
	do
		local mlist = msquares.color(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, OCEAN_COLOR, 4, {
			"INVERT", "HEIGHTS"
		})

		WriteOBJ("msquares_color_invert_heights.obj", mlist)
	end

    -- -----------------------------
    -- msquares_color_dual_heights
    -- -----------------------------
	do
		local mlist = msquares.color(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, OCEAN_COLOR, 4, {
			"DUAL", "HEIGHTS", "SNAP", "CONNECT", "SIMPLIFY"
		})

		WriteOBJ("msquares_color_simplify.obj", mlist)
	end
end

local function WriteSVGMulti (filename, meshdata)
	local n, meshes, state = #meshdata, {}, NewStateSVG()

	for i = 1, n do
		local mdata = meshdata[i]

		ReadOpts(state, mdata, true)

		local mesh = {}

		for k, v in pairs(state.mesh_data) do
			mesh[k] = v
		end

		mesh.mesh = mdata.meshlist:GetMesh(mdata.index or 1)

		WritePath(state, mesh.mesh, mesh)
	end

	local svg = EmitSVG(state)
	local file = open(filename, "w")

	if file then
		file:write(svg)
		file:close()
	end
end

local function test_grayscale ()
	local fp = open(system.pathForFile("msquares_island.1024.bin"), "rb")

	if not fp then return end
	
	local pixels = fp:read("*a")
	
	fp:close()

    -- -----------------------------
    -- msquares_gray_default
    -- -----------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD)

		WriteOBJ("msquares_gray_default.obj", mlist)
	end

    -- -----------------------------
    -- msquares_gray_invert
    -- -----------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD, "INVERT")

local cx, cy = display.contentCenterX, display.contentCenterY

for i = 1, #mlist do
	local mesh = mlist:GetMesh(i)
	local points, uvs, verts = mesh:GetPoints(), {}, {}

	for i = 1, #points, mesh:GetDim() do
		local x, y = points[i], points[i + 1]

		uvs[#uvs + 1] = x
		uvs[#uvs + 1] = y
		
		verts[#verts + 1] = cx * (x - .5)
		verts[#verts + 1] = cx * (y - .5)
	end

	display.newMesh(cx, cy, {
		indices = mesh:GetTriangles(), uvs = uvs, vertices = verts, mode = "indexed", zeroBasedIndices = true
	})
end

		WriteOBJ("msquares_gray_invert.obj", mlist)
	end

    -- -----------------------------
    -- msquares_gray_dual
    -- -----------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD, "DUAL")

		WriteOBJ("msquares_gray_dual.obj", mlist)

		WriteSVGMulti("msquares_gray_dual.svg", {
			{ meshlist = mlist, index = 1, color = 0x050b0, fill = .5, stroke = 1 },
			{ meshlist = mlist, index = 2, color = 0x10e050, fill = 1, stroke = 0 },
			{ meshlist = mlist, index = 2, color = 0, fill = 0, stroke = 1 },
			{
				meshlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, -.02, "DUAL"),
				color = 0xFFFFFF, fill = 0, stroke = 1
			}, {
				meshlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, -.06, "DUAL"),
				color = 0xFFFFFF, fill = 0, stroke = .5
			}, {
				meshlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, -.1, "DUAL"),
				color = 0xFFFFFF, fill = 0, stroke = .25
			}
		})
	end

    -- -----------------------------
    -- msquares_gray_heights
    -- -----------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD, "HEIGHTS")

		WriteOBJ("msquares_gray_heights.obj", mlist)
	end

    -- ------------------------------------
    -- msquares_gray_heights_dual_snap
    -- ------------------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD, {
			"HEIGHTS", "DUAL", "SNAP"
		})

		WriteOBJ("msquares_gray_heights_dual_snap.obj", mlist)
	end

    -- ------------------------------
    -- msquares_gray_heights_simplify
    -- ------------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD, {
			"SIMPLIFY"
		})

		WriteOBJ("msquares_gray_heights_simplify.obj", mlist)
	end

    -- --------------------------------------------
    -- msquares_gray_heights_dual_snap_connect
    -- --------------------------------------------
	do
		local mlist = msquares.grayscale(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, THRESHOLD, {
			"HEIGHTS", "DUAL", "SNAP", "CONNECT"
		})

		WriteOBJ("msquares_gray_heights_dual_snap_connect.obj", mlist)
	end

    -- ------------------------------
    -- msquares_gray_multi_simplify
    -- ------------------------------
	do
		local mlist = msquares.grayscale_multi(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, {
			0.0, 0.1
		}, "SIMPLIFY")

		WriteOBJ("msquares_gray_multi_simplify.obj", mlist)
	end

    -- ------------------------------
    -- msquares_gray_multi_everything
    -- ------------------------------
	do
		local mlist = msquares.grayscale_multi(pixels, IMGWIDTH, IMGHEIGHT, CELLSIZE, {
			0.0, 0.1
		}, {
			"SIMPLIFY", "HEIGHTS", "SNAP", "CONNECT"
		})

		WriteOBJ("msquares_gray_multi_everything.obj", mlist)
	end
end

test_color()
test_grayscale()
test_multi()