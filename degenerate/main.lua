--- Test for degenerate triangles in mesh.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local random = math.random

-- Corona globals --
local display = display
local graphics = graphics
local timer = timer

-- Add a bunch of background objects to emphasize that we aren't just blocking the mesh.
for i = 1, 100 do
	local x = random(display.contentWidth)
	local y = random(display.contentHeight)
	local circ = display.newCircle(x, y, random(3, 15))

	circ:setFillColor(random(), random(), random())
end

-- Quad-relative offsets of uvs and positions --
local Indices = { 1, 3, 5, 5, 3, 7 }

-- Mesh dimensions --
local W, H = math.floor(.6 * display.contentWidth), math.floor(.6 * display.contentHeight)

-- Adds a component quad to the mesh
local uvs, vertices = {}, {}

local function AddQuad (v)
	local n = #vertices

	for i = 1, #Indices do
		local j = Indices[i]

		uvs[n + 1], vertices[n + 1] = v[j], v[j] * W
		uvs[n + 2], vertices[n + 2] = v[j + 1], v[j + 1] * H

		n = n + 2
	end
end

-- Cell counts and dimensions --
local NCols, NRows = 5, 7
local ColW, RowH = 1 / NCols, 1 / NRows

-- Load each of the mesh's quads. Keep a record of the offset to each quad
-- so that we can show / hide them.
local offsets = {}

for row = 0, NRows - 1 do
	local y = row * RowH

	for col = 0, NCols - 1 do
		local x = col * ColW

		offsets[#offsets + 1] = #vertices / 2 + 1

		AddQuad{
			x, y, x + ColW, y,
			x, y + RowH, x + ColW, y + RowH
		}
	end
end

-- Create an effect that will hide vertices with negative u-coordinates. On its own,
-- this does nothing that is not better achieved with auto-batched objects using the
-- isVisible flag. However, this snippet can be part of a larger shader.
do
	local kernel = { category = "filter", name = "can_hide" }

	kernel.vertex = [[
		P_POSITION vec2 VertexKernel (P_POSITION vec2 pos)
		{
			return pos * step(0.0, CoronaTexCoord.x);	// map to (0, 0) when u < 0; since
														// all verts in quad do this, both
														// triangles are culled
		}
	]]

	graphics.defineEffect(kernel)
end

-- Add a mesh and install the "can hide" effect
local mesh = display.newMesh{
	x = display.contentCenterX, y = display.contentCenterY,
	mode = "triangles", uvs = uvs, vertices = vertices
}

mesh.fill = { filename = "Image1.jpg", type = "image" }

mesh.fill.effect = "filter.custom.can_hide"

-- Show or hide a quad
local function Show (oi, show)
	local index, mpath = offsets[oi], mesh.path

	for i = index, index + 5 do
		local u, v = mpath:getUV(i)

		u = -1 - u -- show: un-negate u-coordinate, then subtract 1 from it; when hiding, the reverse
				   -- the +/-1 is to account for the u == 0 case

		mpath:setUV(i, u, v)
	end
end

-- Every so often, hide a cell; at the same time, show the previously hidden one.
local Prev

timer.performWithDelay(300, function()
	local index = random(NCols * NRows)

	if Prev then
		Show(Prev, true)
	end

	Prev = index

	Show(index, false)
end, 0)

-- display.setDrawMode("wireframe")