--- Entry point.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

local Dim = 15

local uvs, vertices = {}, {}

local soup = require("soup")

local CellDim = 15--2

local random = math.random

for r = 1, Dim do
	for c = 1, Dim do
		if math.random(40) < 25 then
			local x, y = (c - 1) * CellDim, (r - 1) * CellDim

			vertices[#vertices + 1] = x
			vertices[#vertices + 1] = y
		
			vertices[#vertices + 1] = x + CellDim
			vertices[#vertices + 1] = y
		
			vertices[#vertices + 1] = x
			vertices[#vertices + 1] = y + CellDim

			vertices[#vertices + 1] = x + CellDim
			vertices[#vertices + 1] = y + CellDim
		
			uvs[#uvs + 1] = 0
			uvs[#uvs + 1] = 0--random()
		
			uvs[#uvs + 1] = 0
			uvs[#uvs + 1] = .25--random()
		
			uvs[#uvs + 1] = 0
			uvs[#uvs + 1] = .5--random()

			uvs[#uvs + 1] = 0
			uvs[#uvs + 1] = .75--random()
		end
	end
end

local n = #vertices / 8
local qs = soup.NewQuadSoup(n)

for _ = 1, n do
	qs:Insert()
end

local indices = qs:GetIndices()
local ni, nv = #indices, #vertices

local Layers = 8

for j = 1, Layers do
	local offset = nv * j

	for k = 1, ni do
		indices[#indices + 1] = indices[k] + offset
	end

	for k = 1, nv do
		vertices[#vertices + 1] = vertices[k]
	end

	local v = j / Layers

	for k = 1, nv, 2 do
		uvs[#uvs + 1] = v
		uvs[#uvs + 1] = uvs[k+1]--random()
	end
end

local m = display.newMesh{ mode = "indexed", indices = indices, uvs = uvs, vertices = vertices }

m.x, m.y = display.contentCenterX, display.contentCenterY

--m:setFillColor(0, 1, 0)

local effect = { category = "generator", group = "mesh", name = "fur" }

effect.isTimeDependent = true

local W, Y = 5.75, 9.75

effect.vertex = ([[
	P_POSITION vec2 VertexKernel (P_POSITION vec2 pos)
	{
		pos.x += %f * sin(CoronaTotalTime * CoronaTexCoord.y * 4.75);
		pos.y -= CoronaTexCoord.x * %f;

v_TexCoord.y = step(.5, CoronaTexCoord.y);
v_TexCoord.x = step(.25, CoronaTexCoord.y - v_TexCoord.y * .5);

		return pos;
	}
]]):format(W, Y)

if true then
	effect.fragment = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			return texture2D(CoronaSampler0, uv);
		}
	]]
end

graphics.defineEffect(effect)

m.fill = { type = "image", filename = "Front2.png" }

m.fill.effect = "generator.mesh.fur"
m.alpha = .25

if true then return end

local w, h = m.width + 2 * math.ceil(W), m.height + math.ceil(Y)

local b = graphics.newTexture{ type = "canvas", width = w, height = h }

b:draw(m)

--m.y = m.y + math.ceil(Y)

local k2 = { category = "composite", group = "mesh", name = "combine" }

k2.fragment = [[
	P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
	{
		P_COLOR vec4 color1 = texture2D(CoronaSampler0, uv);
		P_COLOR float fur = texture2D(CoronaSampler1, uv).r;
		P_COLOR vec4 color2 = texture2D(CoronaSampler0, uv + 1.5 * fur * CoronaTexelSize.xy);
		P_COLOR vec4 color3 = texture2D(CoronaSampler0, uv + 1.5 * fur * CoronaTexelSize.yx);

		return .5 * color1 + .25 * color2 + .25 * color3;
	}
]]

graphics.defineEffect(k2)

local r = display.newRect(display.contentCenterX, display.contentCenterY, 200, 200)

r.fill = {
	type = "composite",
	paint1 = { type = "image", filename = "Front2.png" },
	paint2 = { type = "image", filename = b.filename, baseDir = b.baseDir }
}

r.fill.effect = "composite.mesh.combine"

--r:scale(.25, .25)

b:invalidate()

timer.performWithDelay(250, function()
	b:invalidate("cache")
end, 0)