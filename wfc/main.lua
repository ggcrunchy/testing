--- Test for wfc plugin.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local sin = math.sin

-- Plugins --
local Bytemap = require("plugin.Bytemap")
local luaproc = require("plugin.luaproc")
local MemoryBlob = require("plugin.MemoryBlob")
local wfc = require("plugin.wfc")

-- Solar2D globals --
local display = display
local Runtime = Runtime
local transition = transition

--
--
--

luaproc.preload("MemoryBlob", MemoryBlob.Reloader)
luaproc.preload("wfc", wfc.Reloader)

--
--
--

local texture = Bytemap.loadTexture{ filename = "BLURB.png", format = "rgb" }
local image = display.newImageRect(texture.filename, texture.baseDir, 350, 350)

image.x, image.y = 200, 200

-- To be captured:
local data, w, h = texture:GetBytes(), texture.width, texture.height
local out_w, out_h = 256, 256

--
--
--

luaproc.newproc(function()
  local memory_blob = require("MemoryBlob")
  local system = require("system")
  local wfc = require("wfc")

  --
  --
  --

  local wfc_context, err = wfc.overlapping{
    width = w, height = h, component_count = 3,
    data = data,

    output_width = out_w, output_height = out_h,
    tile_width = 3, tile_height = 3,

    expand_input = true,
    xflip_tiles = true,
    yflip_tiles = true,
    rotate_tiles = true
  }

  if not wfc_context then
    print(err)

  --
  --
  --

  else
    luaproc.sleep(1000)

    local output_blob = memory_blob.New{ resizable = true }

    repeat
      luaproc.alert("alerts", "MSG:Starting attempt...")

      local start_time = system.getTimer()    
      local ok = wfc_context:run()
      local time_report = " (Done after " .. (system.getTimer() - start_time) / 1000 .. " seconds)"

      if ok then
        luaproc.alert("alerts", "MSG:Success!" .. time_report)
        
        local output_ok, w_res, h_res, components = wfc_context:get_output_image(output_blob)

        if output_ok then
          assert(w_res == out_w)
          assert(h_res == out_h)
          assert(components == 3)

          luaproc.alert("alerts", output_blob:Submit())
        else
          print(w_res)
        end

        break
      else
        luaproc.alert("alerts", "MSG:Contradiction: trying again in 3 seconds" .. time_report)
      end
    
      luaproc.sleep(3000)
    until luaproc.wants_to_close()
  end
  
  wfc_context:close()
end)

--
--
--

local output = MemoryBlob.New{ resizable = true }
local message = display.newText("Preparing", display.contentCenterX, 50, native.systemFontBold, 35)

luaproc.get_alert_dispatcher():addEventListener("alerts", function(event)
  local payload = event.payload

  if payload:sub(1, 4) == "MSG:" then -- normal message?
    message.text = payload:sub(5)
  else -- image ready?
    local out_texture = Bytemap.newTexture{ width = out_w, height = out_h, format = "rgb" }

    output:Sync(payload)
    out_texture:SetBytes(output)

    local result = display.newImageRect(out_texture.filename, out_texture.baseDir, 800, 800)
    
    result.x, result.y = 1000, 500
  end
end)

--
--
--

local ball

Runtime:addEventListener("enterFrame", function(event)
  local y = 700 + sin(event.time / 500) * 300

  if ball then
    ball.y = y
  else
    ball = display.newCircle(100, y, 20)

    ball:setFillColor(1, 0, .3)

    ball.alpha = 0

    transition.to(ball, { alpha = 1 })
  end
end)