--- libnoise test.

--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
--

-- Plugins --
local libnoise = require("plugin.libnoise")

--
--
--

local Example = 8

display.newRect(display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight):setFillColor(.1, .3, .7)

local my_module

local base_flat_terrain, flat_terrain, mountain_terrain

if Example < 5 or Example == 8 then
	my_module = libnoise.Perlin()

	--my_module:SetOctaveCount(6)
	--my_module:SetFrequency(1.0)
	--my_module:SetPersistence(0.5)
	if Example == 8 then
		my_module:SetOctaveCount(10)
	end
else
	base_flat_terrain = libnoise.Billow()
	flat_terrain = libnoise.ScaleBias()
	mountain_terrain = libnoise.RidgedMulti()

	base_flat_terrain:SetFrequency(2.0)
end

local height_map = libnoise.NoiseMap()
local height_map_builder

if Example == 8 then
	height_map_builder = libnoise.NoiseMapBuilderSphere()
else
	height_map_builder = libnoise.NoiseMapBuilderPlane()
end

local final_terrain, terrain_type

if base_flat_terrain then
	flat_terrain:SetSourceModule(1, base_flat_terrain)
	flat_terrain:SetScale(.125)
	flat_terrain:SetBias(-.75)

	terrain_type = libnoise.Perlin()

	terrain_type:SetFrequency(0.5)
	terrain_type:SetPersistence(0.25)

	final_terrain = libnoise.Select()

	final_terrain:SetSourceModule(1, flat_terrain)
	final_terrain:SetSourceModule(2, mountain_terrain)
	final_terrain:SetControlModule(terrain_type)
	final_terrain:SetBounds(0, 1000)
	final_terrain:SetEdgeFalloff(.125)

	if Example == 6 then
		local terrain_selector = final_terrain

		final_terrain = libnoise.Turbulence()

		final_terrain:SetSourceModule(1, terrain_selector)
		final_terrain:SetFrequency(4.0)
		final_terrain:SetPower(0.125)
	end

	height_map_builder:SetSourceModule(final_terrain)
else
	height_map_builder:SetSourceModule(my_module)
end

height_map_builder:SetDestNoiseMap(height_map)

if Example == 8 then
	height_map_builder:SetDestSize(512, 256)
	height_map_builder:SetBounds(-90.0, 90.0, -180.0, 180.0)
else
	local dx = 0

	if Example >= 5 then
		dx = 4
	end

	height_map_builder:SetDestSize(256, 256)
	height_map_builder:SetBounds(2.0 + dx, 6.0 + dx, 1.0, 5.0)
end

height_map_builder:Build()

local renderer = libnoise.RendererImage()
local image = libnoise.Image()

renderer:SetSourceNoiseMap(height_map)
renderer:SetDestImage(image)

if true then -- color?
	renderer:ClearGradient ()

	if Example < 5 or Example == 8 then
		renderer:AddGradientPoint(-1.0000, libnoise.Color(  0,   0, 128/255, 1)) -- deeps
		renderer:AddGradientPoint(-0.2500, libnoise.Color(  0,   0, 1, 1)) -- shallow
		renderer:AddGradientPoint( 0.0000, libnoise.Color(  0, 128/255, 1, 1)) -- shore
		renderer:AddGradientPoint( 0.0625, libnoise.Color(240/255, 240/255,  64/255, 1)) -- sand
		renderer:AddGradientPoint( 0.1250, libnoise.Color( 32/255, 160/255,   0, 1)) -- grass
		renderer:AddGradientPoint( 0.3750, libnoise.Color(224/255, 224/255,   0, 1)) -- dirt
		renderer:AddGradientPoint( 0.7500, libnoise.Color(128/255, 128/255, 128/255, 1)) -- rock
		renderer:AddGradientPoint( 1.0000, libnoise.Color(1, 1, 1, 1)) -- snow
	else
		renderer:AddGradientPoint(-1, libnoise.Color( 32/255, 160/255,   0, 1)) -- grass
		renderer:AddGradientPoint(-.25, libnoise.Color(224/255, 224/255,   0, 1)) -- dirt
		renderer:AddGradientPoint( .25, libnoise.Color(128/255, 128/255, 128/255, 1)) -- rock
		renderer:AddGradientPoint( 1, libnoise.Color(1, 1, 1, 1)) -- snow
	end
end

if true then -- lighting?
	renderer:EnableLight()
	renderer:SetLightContrast(3.0) -- Triple the contrast
	renderer:SetLightBrightness(2.0) -- Double the brightness
end

renderer:Render()

local out = display.newImageRect(image.filename, image.baseDir, 128, 128)

out.x, out.y = display.contentCenterX, display.contentCenterY
out.yScale = -1
--[[
timer.performWithDelay(150, function(event)
	local x, y = (-.5 + math.random()) * .5, (-.5 + math.random()) * .5

	if Example == 6 and event.count % 8 == 0 then
		final_terrain:SetFrequency(math.random(8))
		final_terrain:SetPower(2^-math.random(32))
	end

	height_map_builder:SetBounds(2.0 + x, 6.0, 1.0 + y, 5.0)
	height_map_builder:Build()
	renderer:Render()
	image:invalidate()
end, 0)
--]]