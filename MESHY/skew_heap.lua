#!/usr/bin/env lua
-- http://lua-users.org/lists/lua-l/2008-03/msg00674.html

local assert = assert

local M = {}

--
--  Merge function implementing a skew heap.
--  The result is written to r.right.
--
--  Skew heaps have an amortized complexity of O(log n)
--  and are simple to implement. The data structure
--  is a heap-ordered binary tree. The basic operation
--  is the merge operation which merges two heaps into one.
--
local function skew_merge(a, b, r)
    if not b then
	r.right = a
    else
	while a do
	    if a.prio <= b.prio then
		r.right, r = a, a
		a.left, a = a.right, a.left
	    else
		r.right, r = b, b
		b.left, a, b = b.right, b.left, a
	    end
	end
	r.right = b
    end
end

--
--  Empty test
--
function M.empty(Q)
    return Q.right == nil
end


--
--  Insert into queue.
--  Merge the new node indo the heap.
--
function M.insert(Q, v)
    skew_merge(Q.right, v, Q)
end


--
--  Remove from queue
--
--  Take the root from the heap. Merge its two children to
--  obtain the new root
--
function M.remove(Q)
    local r = Q.right
    assert(r ~= nil, "remove called on empty queue")
    skew_merge(r.left, r.right, Q)
    r.left, r.right = nil, nil
    return r
end

return M

