-- Standard library imports --
local huge = math.huge
local ipairs = ipairs
local rad = math.rad
local remove = table.remove
local setmetatable = setmetatable
local sin = math.sin
local sqrt = math.sqrt
local type = type

-- Modules --
local circle = require("rectangle")
local cubic = require("cubic")
local integrators = require("integrators")
local skew_heap = require("skew_heap")

-- Kernels --
--require("kernels")

-- Corona globals --
local display = display

-- Exports --
local M = {}

-- --
local Surface = {}

Surface.__index = Surface

--
function Surface:GetDims ()
	local mesh = self.m_mesh

	return mesh.width, mesh.height
end

--- DOCME
function Surface:GetHeight (h)
	if type(h) ~= "number" then -- h is an object?
		h = h.path.radius
	end

--	return sin(rad(self.m_mesh.fill.effect.inclination)) * h
	return h
end

--- DOCME
function Surface:GetMesh ()
	return self.m_mesh
end

--- DOCME
function Surface:Release (indices)
	if indices then
		--
		local mpath, used = self.m_mesh.path, self.m_used

		for i = 1, #indices, 3 do
			local index = indices[i]

			mpath:setVertex(index, indices[i + 1], indices[i + 2])

			used[index] = nil
		end
--[[
		--
		self.m_circle_stash:insert(indices.m_circle)

		indices.m_circle = nil]]
	end
end

--- DOCME
function Surface:SetPos (x, y)
	local mesh = self.m_mesh

	mesh.x, mesh.y = x, y
end

--- DOCME
function Surface:SetSources (name)
	local fill = {
		type = "image", filename = name
	}

	--
	self.m_fill, self.m_mesh.fill = fill, fill
end

-- --
local Integrand, Poly = cubic.LineIntegrand{}

-- --
local Heap = {}

-- --
local Stash = {}

-- --
local N = 20

--
local function GetStashedObject (SS, name, layer, effect)
	local group, object = SS[name]
	local n = group.numChildren

	if n > 0 then
		object = group[n]

		SS[layer]:insert(object) -- todo: sort
	else
		object = display.newRect(SS[layer], 0, 0, 100, 100)

		object.fill = SS.m_fill

		object.fill.effect = effect
	end

	return object
end

--- DOCME
function Surface:Stretch (u, v, radius, depth)
	local interior = self.m_interior

	if interior and radius > 0 then
		local mesh = self.m_mesh
		local mpath, used, r2, x, y = mesh.path, self.m_used, radius^2, u * mesh.width, v * mesh.height

		--
		for _, index in ipairs(interior) do
			if not used[index] then
				local px, py = mpath:getVertex(index)
				local dist2 = (px - x)^2 + (py - y)^2

				if dist2 <= r2 then
					local entry = remove(Stash) or {}

					entry.index, entry.px, entry.py, entry.prio, used[index] = index, px, py, dist2, true

					skew_heap.insert(Heap, entry)
				end
			end
		end

		--
		if not skew_heap.empty(Heap) then
			--
			local dx, dy = radius, depth
			local ax, ay = 2 * (1 - dx), 2 * dy
			local bx, by = -1.5 * ax, -1.5 * ay

			cubic.SetPolyFromCoeffs(Poly, ax, ay, bx, by, 1, 0)

			local s, sfrac, t, dt = integrators.Romberg(Integrand, 0, 1), huge, 1, 1 / N
			local group, ir, sl, sr = {}, 1 / radius

			while not skew_heap.empty(Heap) do
				local entry = skew_heap.remove(Heap)

				--
				group[#group + 1] = entry.index
				group[#group + 1] = entry.px
				group[#group + 1] = entry.py

				--
				if entry.prio > 1e-12 then	
					local len = sqrt(entry.prio)

					--
					local tfrac = 1 - len * ir

					while tfrac < sfrac do
						t = t - dt
						sl, sr = integrators.Romberg(Integrand, 0, t), sl or s
						sfrac = sl / s
					end

					local offset = (sfrac - sl) / (sr - sl)
					local scale = 1 - (t + offset * dt)

					-- Reverse
					mpath:setVertex(entry.index, x + scale * (entry.px - x), y + scale * (entry.py - y))
				end

				--
				Stash[#Stash + 1] = entry
			end
--[[
			--
			local circle = GetStashedObject(self, "m_circle_stash", "m_front", "composite.custom.ellipse")

			--
			local h = self:GetHeight(depth)
			local xc, yc = mesh:localToContent(x, self:GetHeight(y))
local hw = mesh.width / 2

if x + hw < radius then
	radius = x + hw
end

if x + radius > hw then
	radius = hw - x
end

			--
			circle.x, circle.y = xc, yc

			circle.path.width, circle.path.height = 2 * radius, 2 * self:GetHeight(radius)

			group.m_circle = circle

			--
			local ceffect = circle.fill.effect
local uvr = .5 * radius / self.m_radius
			ceffect.cu = u + .5
			ceffect.cv = v + .5
			ceffect.uv_radius = uvr
]]
-- TODO: Clamp the radius...
			--
			return group
		end
	end
end

--- DOCME
function M.New (ncols, nrows, w, h, opts)
	--
	local interior, parent = {}

	if opts then
		parent = opts.parent
	end

	--
	local mesh_layer = display.newGroup()
	local front = display.newGroup()

	--
	local circle_stash = display.newGroup()

	circle_stash.isVisible = false

	--
	if parent then
		parent:insert(mesh_layer)
		parent:insert(front)
		parent:insert(circle_stash)
	end

	--
	local uvs, vertices, indices = circle.NewLattice(ncols, nrows, w, h, interior)
	local mesh = display.newMesh(mesh_layer, {
		mode = "indexed", indices = indices, uvs = uvs, vertices = vertices
	})

	mesh:translate(mesh.path:getVertexOffset())

	--
	return setmetatable({
		m_circle_stash = circle_stash,
		m_front = front,
		m_interior = interior,
		m_mesh = mesh,
		m_radius = sqrt(.25 * (w^2 + h^2)),
		m_used = {}
	}, Surface)
end

-- ^^ TODO: Better grouping, "finalize"

-- Export the module.
return M