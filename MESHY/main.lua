-- Modules --
local circle = require("rectangle")
local stretchy_surface = require("stretchy_surface")

-- Kernels --
--require("kernels")

local radius = display.contentCenterX - 20

local ncols, nrows = 27, 29
local membrane = stretchy_surface.New(ncols, nrows, display.contentWidth, display.contentHeight - radius)

membrane:SetSources("Front3.png")
membrane:SetPos(display.contentCenterX, display.contentHeight - radius)

-- display.setDrawMode("wireframe", true)
-- transition.to(membrane:GetMesh().fill.effect, { inclination = 90, time = 1000 })


local pad = display.newCircle(display.contentCenterX, 90, 70)

pad:addEventListener("touch", function(event)
	local this = event.target
	local tbounds = this.contentBounds
	local mx, w = (tbounds.xMin + tbounds.xMax) / 2, tbounds.xMax - tbounds.xMin
	local my, h = (tbounds.yMin + tbounds.yMax) / 2, tbounds.yMax - tbounds.yMin
	local x, y = (event.x - mx) / w, (event.y - my) / h

	if event.phase == "began" then
		local t, group = 0


		local proxy = setmetatable({}, {
			__index = function()
				return t
			end,
			__newindex = function(_, _, v)
				t = v

				membrane:Release(group)

				group = membrane:Stretch(x, y, t * 345, t * 600)
			end
		})

		transition.to(proxy, {
			t = 1,
			transition = easing.continuousLoop,
			time=1800,
			onComplete = function() membrane:Release(group) end
		})
		--proxy.t = .2


	elseif event.phase == "ended" or event.phase == "cancelled" then
		-- ?
	end

	return true
end)

-- make some sort of neighborhood search data structure

-- search neighborhood, pull toward center according to area of influence