-- Corona globals --
local graphics = graphics

-- Kernel to etch out an elliptical approximation the surface's sunken interior.
do
	local kernel = { category = "composite", name = "ellipse" }

	kernel.vertexData = {
		{ name = "cu", index = 0 },
		{ name = "cv", index = 1 },
		{ name = "uv_radius", index = 2 },
		{ name = "vradius", index = 3 }
	}

	kernel.fragment = [[
		P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
		{
			P_POSITION vec2 pos = 2. * uv - 1.;
			P_UV float scale = step(dot(pos, pos), 1.); // The dot product is A * x^2 + B * y^2
			P_UV float y = -sqrt(1. - pos.x * pos.x), uvy = smoothstep(y, -2. * y, pos.y * .44);
			P_UV vec2 offset = vec2(pos.x, y) * CoronaVertexUserData.z;

			uv = CoronaVertexUserData.xy + mix(offset, vec2(0.), uvy);

			uv.y = 1. - uv.y;

			P_COLOR vec3 below = texture2D(CoronaSampler0, uv).rgb;
			P_COLOR vec4 above = texture2D(CoronaSampler1, uv);

			return CoronaColorScale(vec4(mix(below, above.rgb, above.a * exp(-uvy * 4.1)), 1.) * scale);
		}
	]]

	graphics.defineEffect(kernel)
end