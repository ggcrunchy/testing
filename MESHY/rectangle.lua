-- Exports --
local M = {}

--
local function AddVertex (uvs, vertices, x, y, w, h)
	uvs[#uvs + 1] = x / w + .5
	uvs[#uvs + 1] = y / h + .5

	vertices[#vertices + 1] = x
	vertices[#vertices + 1] = y
end

-- Add the indices as a triangle
local function AddTri (indices, i1, i2, i3)
	indices[#indices + 1] = i1
	indices[#indices + 1] = i2
	indices[#indices + 1] = i3
end

-- Add the indices in quad-sized blocks (0, 2, 1; 1, 2, 3)
local function AddQuad (indices, i1, i2, i3, i4)
	AddTri(indices, i1, i3, i2)
	AddTri(indices, i2, i3, i4)
end

--- DOCME
function M.NewLattice (ncols, nrows, w, h, interior)
	local uvs, vertices = {}, {}

	--
	local x, y, dw, dh = -w / 2, -h / 2, w / ncols, h / nrows

	for i = 1, nrows + 1 do
		for j = 1, ncols + 1 do
			AddVertex(uvs, vertices, x + (j - 1) * dw, y, w, h)
		end

		y = y + dh
	end

	--
	local indices, index, stride = {}, 1, ncols + 1

	for row = 1, nrows do
		local add_ok = row > 1 and row < nrows

		for j = 1, ncols do
			AddQuad(indices, index, index + 1, index + stride, index + stride + 1)

			if add_ok and j > 1 and j < ncols then
				interior[#interior + 1] = index
			end

			index = index + 1
		end

		index = index + 1
	end

	return uvs, vertices, indices
end

-- Export the module.
return M