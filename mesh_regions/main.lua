--- Test for amalgamated multiple regions with varying properties into one mesh.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]
display.setDefault("isShaderCompilerVerbose",true)
-- Corona globals --
local display = display
local graphics = graphics

-- Quad-relative offsets of uvs and positions --
local Indices = { 1, 3, 5, 5, 3, 7 }

-- Stock "full image" coordinates for uv quad --
local UV = {
	0, 0, 1, 0,
	0, 1, 1, 1
}

-- Adds a component quad to the mesh
local uvs, vertices = {}, {}

local function AddQuad (v, region_index)
	local offset = 2 * (region_index - 1) -- double the offset so coordinates of 1 can be isolated by mod(x, 2)

	for i = 1, #Indices do
		local j = Indices[i]

		uvs[#uvs + 1] = UV[j] + offset
		uvs[#uvs + 1] = UV[j + 1]

		vertices[#vertices + 1] = v[j]
		vertices[#vertices + 1] = v[j + 1]
	end
end

-- Combine multiple regions, with different properties, into one mesh.
AddQuad({
	20, 20, 150, 20,
	20, 70, 150, 70
}, 1)

AddQuad({
	200, 50, 350, 60,
	180, 80, 350, 90
}, 1)

AddQuad({
	100, 100, 150, 100,
	100, 200, 150, 200
}, 2)

AddQuad({
	250, 200, 350, 200,
	250, 250, 350, 250
}, 3)

AddQuad({
	10, 200, 75, 200,
	10, 250, 75, 250
}, 3)

AddQuad({
	200, 80, 250, 80,
	200, 120, 250, 120
}, 4)

-- Very small bitwise color palette --
local Red = 1
local Green = 2
local Blue = 4
-- ... any combinations of above, or 0 for black
local White = Red + Green + Blue

-- Create an effect that will assign different properties to various regions of the
-- mesh. On its own, this does nothing that is not better achieved with auto-batched
-- objects using vertex userdata. However, we can apply this to much more general
-- objects, e.g. non-quads (in particular, ones that will be far more dynamic than
-- those created by display.newPolygon) or those with shifting texture coordinates.
-- In this case we only have 16 possible colors each; uniform userdata and / or a
-- lookup texture can be used for larger data needs.
do
	local kernel = { category = "filter", name = "regioned" }

	kernel.vertexData = {
		{ name = "color1", default = White, index = 0 },
		{ name = "color2", default = White, index = 1 },
		{ name = "color3", default = White, index = 2 },
		{ name = "color4", default = White, index = 3 }
	}

	kernel.vertex = [[
		varying P_UV vec2 v_UV;
		varying P_COLOR vec3 v_Color;

		P_POSITION vec2 VertexKernel (P_POSITION vec2 pos)
		{
			v_UV = mod(CoronaTexCoord, 2.);

			int index = int(CoronaTexCoord - v_UV) / 2; // undo offset doubling
P_UV vec4 vv = vec4(index == 0 ? 1. : 0., index == 1 ? 1. : 0., index == 2 ? 1. : 0., index == 3 ? 1. : 0.);
			P_COLOR float bits = dot(CoronaVertexUserData, vv);//CoronaVertexUserData[index];
			P_COLOR float red = mod(bits, 2.);	// mask off red (low bit)
			P_COLOR float blue = step(4., bits);// has blue? (high bit)
			P_COLOR float green = (bits - 4. * blue - red) / 2.;// remove red and blue contributions, shift green out

			v_Color = vec3(red, green, blue);

			return pos;
		}
	]]

	kernel.fragment = [[
		varying P_UV vec2 v_UV;
		varying P_COLOR vec3 v_Color;

		P_COLOR vec4 FragmentKernel (P_UV vec2 _)
		{
			return texture2D(CoronaSampler0, v_UV) * vec4(v_Color, 1.);
		}
	]]

	graphics.defineEffect(kernel)
end

-- Show the mesh, assigning colors to a few regions.
local mesh = display.newMesh{
	x = display.contentCenterX, y = display.contentCenterY,
	mode = "triangles", uvs = uvs, vertices = vertices
}

mesh.fill = { filename = "Image1.jpg", type = "image" }

mesh.fill.effect = "filter.custom.regioned"

mesh.fill.effect.color1 = Red + Blue
mesh.fill.effect.color2 = Green
mesh.fill.effect.color3 = Blue

-- display.setDrawMode("wireframe")