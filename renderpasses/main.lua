local commands = {}
for i = 1, 70 do
    commands[#commands + 1] = "x"
end
for _, pos in ipairs{ 1, 7, 14, 23, 25, 28, 29, 43, 46, 50, 52, 54, 61, 64 } do
    commands[pos] = "begin"
end
for _, pos in ipairs{ 6, 13, 22, 31, 33, 37, 42, 48, 49, 57, 58, 63, 67, #commands } do
    commands[pos] = "end"
end
local jumps, stack, prev, rejoined_lower_level = {}, {}, {}
for i, v in ipairs(commands) do
	local left_lower_level = rejoined_lower_level
	rejoined_lower_level = nil
    if v == "begin" then
        local new = {}
        if prev.ended_at then -- previous swath was on this stack level, or cleared the stack?
            jumps[prev.ended_at] = i
        else -- this swath is within another still in progress
            prev.will_jump_to = i
        end
        if #stack > 0 then
            new.left_lower_level = left_lower_level or (i - 1)
        end
        prev, returned = new
        stack[#stack + 1] = new
    elseif v == "end" then
        local cell = table.remove(stack)
        if cell.left_lower_level then
			rejoined_lower_level = cell.left_lower_level
            jumps[rejoined_lower_level] = i + 1
        end
        if cell.will_jump_to then
            jumps[i] = cell.will_jump_to
        end
        cell.ended_at = i
    end
end
jumps[prev.ended_at] = "quit"
local index = 1
local cc=0
while true do
    local command = commands[index]
cc=cc+1
if cc==80 then print(":(") return end
    print("INSTRUCTION:", index, command)
    if jumps[index] then
        index = jumps[index]
        if index == "quit" then
            return
        else
            print("JUMPED TO INSTRUCTION:", index)
        end
    else
        index = index + 1
    end
end