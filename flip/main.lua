        --snap to use as a card or tile (Slightly larger to have the desidered border)
	local card = display.newSnapshot( 230, 230 )
	card.x = display.contentCenterX
	card.y = display.contentCenterY

	--snap group
	local cardG = card.group

	--background (used as a card board)
	local bg = display.newRect( cardG, 0, 0, 200, 200 )
	bg:setFillColor( 1, 1, 1, 0 )
	bg.strokeWidth = 20
	bg:setStrokeColor(.7, .7, .7, 1)
--[[
	--hearts suit
	local hearts = display.newImageRect( cardG, "hearts.png", 60, 60 )
	hearts.x = bg.x - (bg.width*0.5) + (hearts.width*0.5) + bg.strokeWidth

	--diamonds suit
	local diamonds = display.newImageRect( cardG, "diamonds.png", 60, 60 )
	diamonds.x = bg.x + (bg.width*0.5) - (diamonds.width*0.5) - bg.strokeWidth

	--clubs suit
	local clubs = display.newImageRect( cardG, "clubs.png", 60, 60 )
	clubs.y = bg.y - (bg.height*0.5) + (clubs.height*0.5) + bg.strokeWidth

	--spades suit
	local spades = display.newImageRect( cardG, "spades.png", 60, 60 )
	spades.y = bg.y + (bg.height*0.5) - (spades.height*0.5) - bg.strokeWidth

	--centre of card
	local centre = display.newRect( cardG, 0, 0, 60, 60 )
]]
local cc=display.newImageRect(cardG, "cropped.png", bg.width, bg.height)
cc.x, cc.y = bg.x, bg.y
	--button that serves the user to turn the card
	local rotateButton = display.newRect( 0, 0, 30, 50 )
	rotateButton:setFillColor( 0, 1, 0 )
	rotateButton.x =  rotateButton.width
	rotateButton.y = display.contentCenterY

	--rotateButton touch event
	function rotateButton:touch( event )

        if ( event.phase == "began" ) then
            display.getCurrentStage():setFocus(self, event.id)
            self.isFocus = true


         elseif(self.isFocus) then

			if event.phase == "moved" then

				--distance path with his finger by the user
				local distanceTraveled = (event.x-event.xStart)

				--distance in proportion to the width of the paper
				local distanceInProportion = (distanceTraveled/card.width)*2

				print(distanceInProportion)


				if(distanceInProportion > 0) and (distanceInProportion < 2) then

					--tilting the paper by changing the scale (old method)
					--card.xScale = 1-distanceInProportion

					--tilting the paper with the new effect
					--I multiplied by seven only to see the operation but do not understand how to calculate the right number
					card.fill.effect.t = distanceInProportion / 4--*7
				end


		     elseif event.phase == "ended" or event.phase == "cancelled" then
    	      	display.getCurrentStage():setFocus( self, nil )
      			self.isFocus = false

	        end
	    end

     	return true
	end
	rotateButton:addEventListener( "touch" )






	local kernel = { category = "filter", name = "rotate" }
		
	kernel.vertexData = {
	  { index = 0, name = "t", default = 0 },
	  { index = 1, name = "thickness", default = .075 },
	}
		
	kernel.fragment = [[
		const P_UV float PI = 180.;//3.14159;
		const P_UV float WIDTH = 13.;

		#define NPIXELS CoronaTexelSize.z * WIDTH
		#define TO_NEIGHBOR CoronaTexelSize.z * 14.

		P_UV float LessThan (P_UV float threshold, P_UV float x)
		{
		    return step(abs(x), threshold); // 1.0 if true, 0.0 otherwise
		}

		P_UV float MoreThan (P_UV float threshold, P_UV float x)
		{
		    return step(-abs(x), threshold); // 1.0 if true, 0.0 otherwise (the negative just accounts for exactly equal values)
		}

		P_UV float ProjectX (P_UV float x, P_UV float denom)
		{
			return .5 + (x - .5) / denom;
		}

		P_UV float InBounds (P_UV vec2 uv)
		{
		    P_UV vec2 from_center = step(abs(uv - .5), vec2(.4875)); // 1.0 if inside [0, 1] along axis, 0.0 otherwise (done for x and y)
		    
		    return min(from_center.x, from_center.y); // If either is 0.0, we're out-of-bounds; otherwise we're inside
		}

		P_COLOR vec4 FragmentKernel( P_UV vec2 texCoord )
		{
		    P_UV float angle = radians(CoronaVertexUserData.x * 2. * PI);
		    P_UV float cosa = cos(angle), sina = sin(angle), xcur = texCoord.x;
		    P_UV float denom = sign(cosa) * max(abs(cosa), 1e-4); // avoid divides by 0 to avoid driver issues, etc.
		    P_UV float off_edge = InBounds(texCoord);

		    texCoord.x = ProjectX(texCoord.x, denom);

		    P_COLOR vec4 pixel = texture2D(CoronaSampler0, texCoord) * off_edge;
		    P_UV float neighbor_x = texCoord.x - CoronaVertexUserData.y * sina; // look a few pixels to the left or right (depending on angle and distance from sideways)
			P_UV float at_center_and_sideways = MoreThan(.9975, sina) * LessThan(NPIXELS / 2., xcur - .5);  // the multiply is basically an "and"
		    																							 	// if both predicates return 1.0, it's 1.0
		    																								// otherwise it's 0.0
		    texCoord.x = mix(neighbor_x, .5, at_center_and_sideways); // "interpolate" (using our 0.0 or 1.0 result) to pick a value
		    
		    P_COLOR vec4 neighbor = texture2D(CoronaSampler0, texCoord) * InBounds(texCoord);

			return CoronaColorScale(mix(mix(vec4(0.), vec4(1., 0., 0., 1.), neighbor.a), pixel, pixel.a));
		}
	]]
		
	graphics.defineEffect(kernel)
	
	--Apply the new effect
	card.fill.effect = "filter.custom.rotate"