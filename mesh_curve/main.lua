--- Test for mesh-based curve.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local sqrt = math.sqrt

-- Modules --
local cubic = require("cubic")

-- Corona globals --
local display = display

-- Geometry used to indicate underlying curve --
local GeomGroup = display.newGroup()

-- Add a curve indicator circle
local function Circle (x, y, r, g, b)
	local circ = display.newCircle(GeomGroup, x, y, 6)

	circ:setFillColor(0, 0)
	circ:setStrokeColor(r, g, b)

	circ.strokeWidth = 2
	circ.alpha = .3
end

-- Get a unit normal from a curve tangent
local function Normal (tx, ty)
	local nx, ny = -ty, tx
	local len = sqrt(nx^2 + ny^2)

	return nx / len, ny / len
end

-- Auxiliary point sets --
local PV, QV = {}, {}

-- Points along primary curve --
local Points = {
	{ x = 0, y = 90 }, -- just to guide (parallel) curve
	{ x = 30, y = 120 }, -- just to guide curve
	{ x = 85, y = 130 },
	{ x = 150, y = 110 },
	{ x = 215, y = 120 },
	{ x = 260, y = 260 },
	{ x = 310, y = 380 },
	{ x = 330, y = 460 },
	{ x = 280, y = 490 },
	{ x = 270, y = 540 },
	{ x = 310, y = 590 },
	{ x = 350, y = 660 }, -- just to guide curve
	{ x = 420, y = 690 } -- just to guide (parallel) curve
}

-- Number of quads per curve segment --
local Res = 4

-- Height of auxiliary curve relative to primary one --
local Height = 55

-- Number of quads --
local N = Res * (#Points - 5)

-- Add a point to one of the auxiliary sets
local function AddPrep (v, i, s, t)
	local a, b, c, d = Points[i - 1], Points[i], Points[i + 1], Points[i + 2]
	local px, py = cubic.GetPosition("catmull_rom", a, b, c, d, t)
	local tx, ty = cubic.GetTangent("catmull_rom", a, b, c, d, t)
	local nx, ny = Normal(tx, ty)

	s = s * Height

	local x, y, pr, pg, pb = px + s * nx, py + s * ny, 1, v == PV and 1 or 0, 0

	v[#v + 1] = { x = x, y = y }

	local line = display.newLine(GeomGroup, x, y, px, py)

	line:setStrokeColor(pr, pg, pb)

	line.strokeWidth = 2
	line.alpha = .3

	Circle(x, y, pr, pg, pb)
end

-- Add auxiliary sets, "above" and "below" the primary curve.
AddPrep(PV, 2, 1, 0)
AddPrep(QV, 2, -1, 0)

for i = 3, #Points - 3 do
	AddPrep(PV, i, 1, 0)
	AddPrep(QV, i, -1, 0)
end

AddPrep(PV, #Points - 2, 1, 0)
AddPrep(QV, #Points - 2, -1, 0)

AddPrep(PV, #Points - 2, 1, 1)
AddPrep(QV, #Points - 2, -1, 1)

-- Texture u-coordinate state --
local U, DU = 0, 1 / N

-- Add mesh points' uv and position information
local uvs, vertices = {}, {}

local function AddPoints (px, py, qx, qy)
	uvs[#uvs + 1] = U
	uvs[#uvs + 1] = 0
	uvs[#uvs + 1] = U
	uvs[#uvs + 1] = 1

	vertices[#vertices + 1] = qx
	vertices[#vertices + 1] = qy
	vertices[#vertices + 1] = px
	vertices[#vertices + 1] = py

	U = U + DU
end

-- Get a point along one of the auxiliary curves
local function GetPoint (v, i, t)
	local a, b, c, d = v[i - 1], v[i], v[i + 1], v[i + 2]

	return cubic.GetPosition("catmull_rom", a, b, c, d, t)
end

-- Add points, working from the auxiliary curves
local function Add (i, t)
	local px, py = GetPoint(PV, i, t)
	local qx, qy = GetPoint(QV, i, t)

	AddPoints(px, py, qx, qy)
end

-- Interpolate the two auxiliary sets, adding the points along each curve.
for i = 2, #PV - 2 do
	for j = 0, Res - 1 do
		Add(i, j / Res)
	end
end

Add(#PV - 2, 1)

-- Add the indices in quad-sized blocks (0, 2, 1; 1, 2, 3)
local indices = {}

do
	local j = 1

	for _ = 1, N do
		indices[#indices + 1] = j
		indices[#indices + 1] = j + 2
		indices[#indices + 1] = j + 1
		indices[#indices + 1] = j + 1
		indices[#indices + 1] = j + 2
		indices[#indices + 1] = j + 3

		j = j + 2
	end
end

-- Show the primary curve.
for i = 1, #Points do
	Circle(Points[i].x, Points[i].y, 0, 0, 1)
end

-- Show the textured mesh-based curve.
local mesh = display.newMesh{ mode = "indexed", indices = indices, uvs = uvs, vertices = vertices }

mesh:translate(mesh.path:getVertexOffset())

mesh.fill = { type = "image", filename = "Image1.jpg" }

-- Put the points and lines on top.
GeomGroup:toFront()

-- display.setDrawMode("wireframe")