--- Test for TheoraPlay plugin.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

--[=[
if true then






local tp = require("plugin.theora")
local bytemap = require("plugin.Bytemap")

local bb = bytemap.newTexture{ format = "rgb", width = 640, height = 360 }

local f = tp.newWriter{ filename = "out.ogv", baseDir = system.DocumentsDirectory, width = 640, height = 360 }
local v, e = tp.decodeFromFile("640.ogv")

print("?", v, e)

local image = display.newImageRect(v.filename, v.baseDir, 640, 360)

image.x, image.y = display.contentCenterX, display.contentCenterY

local prev

timer.performWithDelay(20, function()
	local now, diff = system.getTimer()

	if prev then
		diff = now - prev
	end

	v:Step(diff or 0)
	v:invalidate()

	prev = now
end, 0)

timer.performWithDelay(400, function(event)
	if event.count < 2e2 then
		for _ = 1, 300 do
			local x, y = math.random(640), math.random(360)
			local r, g, b = math.random(255), math.random(255), math.random(255)
			local rgb = string.char(r, g, b)

			bb:SetBytes(rgb, { x1 = x, y1 = y })
		end

		f:newFrame(bb:GetBytes())
	else
		f:commit()
	end
end, 2e2)





else




local bytemap = require("plugin.Bytemap")

local bb = bytemap.newTexture{ format = "rgba", width = 640, height = 360 }


local mpeg = require("plugin.mpeg")
local v, e = mpeg.decodeFromFile("bjork-all-is-full-of-love.mpg")
								--"out.mpg", system.DocumentsDirectory)
--local f = mpeg.newEncoder{ filename = "out.mpg", width = 640, height = 360, fps = 25 }

print("?", v, e)

local image = display.newImageRect(v.filename, v.baseDir, 640, 360)

image.x, image.y = display.contentCenterX, display.contentCenterY

local prev

timer.performWithDelay(20, function()
	local now, diff = system.getTimer()

	if prev then
		diff = (now - prev) / 1000
	end

	v:Step(diff or 0)
	v:invalidate()

	prev = now
end, 0)
--[[
timer.performWithDelay(400, function(event)
	if event.count < 2e2 then
		for _ = 1, 300 do
			local x, y = math.random(640), math.random(360)
			local r, g, b, a = math.random(255), math.random(255), math.random(255), math.random(128, 255)
			local rgba = string.char(r, g, b, a)

			bb:SetBytes(rgba, { x1 = x, y1 = y })
		end

		f:newFrame(bb:GetBytes())
	else
		f:commit()
	end
end, 2e2)
--]]







end

]=]

local theora = require("plugin.theora")

local video, err = theora.decodeFromFile("640.ogv") -- <- CHANGE THIS NAME

-- print("?", video, err)
local image = display.newImageRect(video.filename, video.baseDir, 640, 360)

image.x, image.y = display.contentCenterX, display.contentCenterY

local prev

timer.performWithDelay(20, function()
    local now, diff = system.getTimer()

    if prev then
        diff = now - prev
    end

    video:Step(diff or 0)
    video:invalidate()
if not prev then
  
print("!",video.width,video.height)
end
    prev = now
end, 0)

timer.performWithDelay(1000, function(event)
  if not video.isDecoding then
    print("DONE!", event.time)
    timer.cancel(event.source)
  end
end, 0)
