--- Test for morphing plugin.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local open = io.open
local print = print

-- Modules --
local skeleton = require("skeleton")

-- Plugins --
local morphing = require("plugin.morphing")

-- Corona modules --
local json = require("json")

-- Corona globals --
local native = native
local system = system

--
skeleton.Init(false)

--
local CX, CY, W, H = skeleton.GetRegion()

--
local function Contents (name)
	local file, err = open(system.pathForFile(name .. ".json", system.ResourceDirectory), "r")

	if file then
		local contents = json.decode(file:read("*a"))

		file:close()

		return contents
	else
		return nil, err
	end
end

--
local Current

--
local Grid, gerr = Contents(skeleton.GetGridName())
local Target1, terr1 = Contents("t1")
local Target2, terr2 = Contents("t2")

if Grid and Target1 and Target2 then
	for i = 1, skeleton.GetInteriorCount() do
		skeleton.UpdateInteriorCoords(i, morphing.PoissonCoordsFromData(Grid[i]))
	end

	local target1 = morphing.PoissonStateFromData(Target1)
	local target2 = morphing.PoissonStateFromData(Target2)

	Current = target1:Clone()

	skeleton.UpdatePoissonState(Current)

	for i = 1, skeleton.GetPointCount() do
		local pos = Target1[i]

		skeleton.LoadPoint(i, pos[1], pos[2])
	end

--	Current:SetMode("midpoint")
	target1:SetMode("midpoint")
	target2:SetMode("midpoint")

	--
local ramp = {}

for i = 1, skeleton.GetPointCount() do
	ramp[i] = 0

	transition.to(ramp, { [i] = 1, time = math.random(300, 1500), iterations = -1, transition = easing.continuousLoop })
end

	timer.performWithDelay(10, function()
		Current:Interpolate(target1, target2, ramp)

		for i = 1, #Current do
			skeleton.UpdatePoint(i, Current:GetPoint(i))
		end

		skeleton.UpdatesDone()
	end, 0)
else
	if gerr then
		print("Error opening interior data file", gerr)
	end

	if terr1 then
		print("Error opening target data file #1", terr1)
	end

	if terr2 then
		print("Error opening target data file #2", terr2)
	end
end

-- display.setDrawMode("wireframe")