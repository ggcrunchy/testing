--- Common skeleton for morphing tests.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

-- Standard library imports --
local ipairs = ipairs

-- Plugins --
local morphing = require("plugin.morphing")

-- Corona globals --
local display = display

-- Exports --
local M = {}

-- --
local nx, ny = 7, 8

-- --
local ninterx, nintery = 5, 5

-- --
local ncols, nrows = (nx - 1) * (ninterx + 1), (ny - 1) * (nintery + 1) -- ninterx * (nx - 1) + nx - 1, ditto for y

-- --
local indices = {}

-- 
do
	local ni, verts_in_row = 0, ncols + 1

	for row = 1, nrows do
		local roff1 = (row - 1) * verts_in_row
		local roff2 = roff1 + verts_in_row

		for col = 1, ncols do
			-- Triangle 1
			indices[ni + 1] = roff1 + col -- will be ni - 5
			indices[ni + 2] = roff1 + col + 1 -- aka ni + 2, will be ni - 4
			indices[ni + 3] = roff2 + col -- aka ni + 3, will be ni - 3

			-- Triangle 2
			indices[ni + 4] = indices[ni + 2]
			indices[ni + 5] = indices[ni + 3]
			indices[ni + 6] = roff2 + col + 1 -- will be ni

			ni = ni + 6
		end
	end
end

-- --
local vertices = {}

--
local CX, CY = display.contentCenterX, display.contentCenterY

-- --
local W, H = math.floor(.6 * display.contentWidth), math.floor(.6 * display.contentHeight)

-- --
local ColW, RowH = W / ncols, H / nrows

local interior, mesh, pstate = {}

local X0, Y0 = CX - W / 2, CY - H / 2

--
local function UpdatePoint (point, x, y, mode)
	local mx, my

	--
	if mode == "loading" then
		point.x, point.y, mx, my = X0 + x, Y0 + y, x, y
	else
		point.x, point.y, mx, my = x, y, x - X0, y - Y0
	end

	--
	pstate:SetPoint(point.m_nb, { mx, my })

	--
	mesh.path:setVertex(point.m_index, mx, my)

	if mode ~= "update_one" then
		for _, ip in ipairs(interior) do
			mesh.path:setVertex(ip.index, ip.coords:Map(pstate))
		end
	end
end

--
local fbounds

local function BoundaryTouch (event)
	local phase, point = event.phase, event.target

	if phase == "began" then
		display.getCurrentStage():setFocus(point)
	elseif phase == "moved" then
		local x, y = event.x, event.y

		if x >= fbounds.xMin and x <= fbounds.xMax and y >= fbounds.yMin and y <= fbounds.yMax then
			UpdatePoint(point, x, y)
		end
	elseif phase == "ended" or phase == "cancelled" then
		display.getCurrentStage():setFocus(nil)
	end

	return true
end

-- --
local group

-- --
local boundary, claimed = {}, {}

--
local ntotal = 4 * (ncols + nrows) -- [2 * (ncols + 1) + 2 * (nrows - 1)] * 2 components

--- DOCME
function M.Init (show_points)
	group = show_points and display.newGroup() or {}

	do
		local candidates = {}
		local loff, roff, index = ntotal - 2, 0, 1

		for row = 0, nrows do
			local y = row * RowH

			for col = 0, ncols do
				local x = col * ColW

				vertices[#vertices + 1] = x
				vertices[#vertices + 1] = y

				if row == 0 or row == nrows or col == 0 or col == ncols then
					local boff

					if row == nrows or (row > 0 and col == 0) then
						boff, loff = loff, loff - 2
					else
						boff, roff = roff, roff + 2
					end

					candidates[boff + 1] = index
					candidates[boff + 2] = #vertices - 1
				end

				index = index + 1
			end
		end

		local function Elect (ci)
			local index, vi = candidates[ci], candidates[ci + 1]
			local x, y, point = vertices[vi], vertices[vi + 1]

			boundary[#boundary + 1] = x
			boundary[#boundary + 1] = y

			local px, py = X0 + x, Y0 + y

			if show_points then
				point = display.newCircle(group, px, py, 8)

				point:addEventListener("touch", BoundaryTouch)
				point:setFillColor(math.random(), math.random(), math.random())
				point:setStrokeColor(0)

				point.strokeWidth = 2
			else
				point = { x = px, y = py }

				group[#group + 1] = point
			end

			point.m_nb, point.m_index, claimed[index] = #boundary / 2, index, true
		end

		local ci, dx, dy = 1, (ninterx + 1) * 2, (nintery + 1) * 2

		for i = 1, nx do
			Elect(ci)

			if i < nx then -- don't step right on right column
				ci = ci + dx
			end
		end

		for i = 1, ny - 1 do
			ci = ci + dy

			if i < ny - 1 then -- last step is just to prepare the left sweep
				Elect(ci)
			end
		end

		for i = 1, nx do
			Elect(ci)

			if i < nx then -- as with the upper row
				ci = ci + dx
			end
		end

		for _ = 1, ny - 2 do -- just do one fewer step than right side; no need for check
			ci = ci + dy

			Elect(ci)
		end
	end

	local poly = morphing.NewPoly(boundary) -- WithHoles(..., edges)

	pstate = poly:NewPoissonState()

	for i = 1, show_points and group.numChildren or #group do
		local point = group[i]

		pstate:SetPoint(point.m_nb, { point.x - X0, point.y - Y0 })
	end

	do
		local vi, index = 1, 1

		for _ = 1, (nrows + 1) * (ncols + 1) do
			if not claimed[index] then
				local p = { vertices[vi], vertices[vi + 1] }

				interior[#interior + 1] = { index = index, coords = poly:BasicPoissonMVC(p) }
			end
		
			vi, index = vi + 2, index + 1
		end
	end

	local x, y = CX, CY

	if show_points then
		local frame = display.newRect(CX, CY, W, H)

		frame:setFillColor(0, 0)
		frame:setStrokeColor(0, 0, 1)

		frame.strokeWidth = 4

		fbounds = frame.contentBounds

		group:toFront()
	else
		x, y = x + X0, y + Y0
	end

	mesh = display.newMesh{ x = x, y = y, mode = "indexed", vertices = vertices, indices = indices }

	mesh.fill = { filename = "Image1.jpg", type = "image" }
end

--- DOCME
function M.GetGridName ()
	return ("Grid_%ix%i_%ix%i"):format(nx, ninterx, ny, nintery)
end

--- DOCME
function M.GetInteriorCoords (i)
	return interior[i].coords
end

--- DOCME
function M.GetInteriorCount ()
	return #interior
end

--- DOCME
function M.GetMesh ()
	return mesh
end

--- DOCME
function M.GetPointCount ()
	return group.numChildren or #group
end

--- DOCME
function M.GetPoissonState ()
	return pstate
end

--- DOCME
function M.GetRegion ()
	return CX, CY, W, H
end

--- DOCME
function M.LoadPoint (i, x, y)
	UpdatePoint(group[i], x, y, "loading")
end

--- DOCME
function M.UpdateInteriorCoords (i, coords)
	interior[i].coords = coords
end

--- DOCME
function M.UpdatePoint (i, x, y)
	UpdatePoint(group[i], x, y, "update_one")
end

--- DOCME
function M.UpdatesDone ()
	for _, ip in ipairs(interior) do
		mesh.path:setVertex(ip.index, ip.coords:Map(pstate))
	end
end

--- DOCME
function M.UpdatePoissonState (state)
	pstate = state
end

-- Export the module.
return M