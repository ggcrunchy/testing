--- Test for video plugin.

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--
-- [ MIT license: http://www.opensource.org/licenses/mit-license.php ]

local video = require("plugin.video")

local w, h = display.contentWidth, display.contentHeight
local v = video.newVideo{ width = w, height = h, filename = system.pathForFile(
--	"320x240.ogg"
--  "320x240_skeleton_p_cmml.ogv" -- Not working?
--	"chained_streams.ogg"
--		"322x242_not-divisible-by-sixteen-framesize.ogg"
	"ducks_take_off_444_720p25.ogg"
--  "mobile_itu601_i_422.ogg"
--"sign_irene_cif-3qi-b.ogg"
--  "stockholm-vfr.ogg"
-- "videotestsrc-720x576-16-15.ogg" -- Not sure if it's supposed to do anything interesting
		) } -- YOUR FILE HERE
Runtime:addEventListener("vlc_video_done", function(event)
	print(event.object, v)
end)
if v then
	local image = display.newImage(v.filename, v.baseDir, w, h)

	image.x, image.y = display.contentCenterX, display.contentCenterY

	do
		--
		local kernel = { category = "filter", group = "vlc", name = "swap_rb" }

		kernel.fragment = [[
			P_COLOR vec4 FragmentKernel (P_UV vec2 uv)
			{
				return texture2D(CoronaSampler0, uv).bgra;
			}
		]]

		graphics.defineEffect(kernel)
	end

	image.fill.effect = "filter.vlc.swap_rb"
end